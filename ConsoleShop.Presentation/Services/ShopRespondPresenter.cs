﻿using ConsoleShop.Presentation.Extensions;
using ConsoleShop.Presentation.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using static System.Console;

namespace ConsoleShop.Presentation.Types
{
    /// <summary>
    /// A public static class that represents a service to present all shop application responses in a command line.
    /// </summary>
    public static class ShopRespondPresenter

    {
        /// <summary>
        /// Presents all messages which comes in a form of a string.
        /// </summary>
        /// <param name="message">A message to be presented.</param>
        public static void PrintToConsole(string message)
        {
            if (message.Contains("WELDERPOINT"))
            {
                ForegroundColor = ConsoleColor.DarkYellow;
                Write(message.Substring(0, message.IndexOf("WELDERPOINT")));
                ForegroundColor = ConsoleColor.Black;
                BackgroundColor = ConsoleColor.Yellow;
                Write("WELDERPOINT");
                BackgroundColor = ConsoleColor.Black;
                ForegroundColor = ConsoleColor.DarkYellow;
                Write("");
                Write(message.Substring(message.IndexOf("WELDERPOINT") + 11));
                WriteLine();
                ForegroundColor = ConsoleColor.Cyan;

            }
            else
            {
                ForegroundColor = ConsoleColor.DarkYellow;
                WriteLine(message);
                ForegroundColor = ConsoleColor.Cyan;
            }
        }

        /// <summary>
        /// Presents all messages which comes in a form of a dictionary.
        /// </summary>
        /// <typeparam name="T">A generic type for values of different fields.</typeparam>
        /// <param name="message">A message to be presented.</param>
        public static void PrintToConsole<T>(Dictionary<string, T> message)
        {
            ForegroundColor = ConsoleColor.DarkYellow;

            if (message.Keys.First().Contains("Result")) WriteLine(message.Keys.First() + message.Values.First());

            else if (message.Keys.Any(key => key.Contains('|'))) PresentTreeInfo(message);

            else PresentSingleInfo(message);

            ForegroundColor = ConsoleColor.Cyan;
        }

        /// <summary>
        /// Formats multilined message from multiple values of a simple key.
        /// </summary>
        /// <param name="itemKey">A key from a dictionary that represents compound data.</param>
        /// <param name="itemValue">Compound data to be transformed to multilined message.</param>
        /// <param name="itemNote">An instance of a StringBuilder to work with.</param>
        /// <param name="substString">Substitution string for duplicated data.</param>
        /// <returns>A multilined message.</returns>
        private static string HandleLines(string itemKey, string itemValue, StringBuilder itemNote, in string substString)
        {
            string itemDescription = itemValue;
            itemNote.Append(itemKey + ": ");
            List<string> lines = itemDescription.FormatToLines(10);
            if (lines.Count == 1)
            {
                itemNote.Append(lines.First());
            }
            else
            {
                itemNote.AppendLine();
                for (int i = 0; i < lines.Count; i++)
                {
                    if (i == lines.Count - 1)
                        itemNote.Append(substString + "| " + lines[i]);
                    else
                        itemNote.AppendLine(substString + "| " + lines[i]);
                }
            }
            return itemNote.ToString();
        }

        /// <summary>
        /// Presents compound messages.
        /// </summary>
        /// <typeparam name="T">A generic type for dictionary values.</typeparam>
        /// <param name="message">A message to be presented.</param>
        private static void PresentTreeInfo<T>(Dictionary<string, T> message)
        {
            List<string> printedItems = new List<string>();
            string substString = String.Empty;
            StringBuilder itemNote = new StringBuilder();
            foreach (KeyValuePair<string, T> item in message)
            {
                if (!item.Key.Contains('|'))
                {
                    WriteLine(item.Key + ": " + item.Value);
                    continue;
                }                

                string resultItemNote = HandleLines(item.Key, item.Value.ToString(), itemNote, in substString);

                if (printedItems.Any(itemKey => resultItemNote.Contains(itemKey)))
                {
                    substString = new string(' ', printedItems.First(itemKey => resultItemNote.Contains(itemKey)).Length);

                    resultItemNote = substString + resultItemNote.Substring(resultItemNote.IndexOf('|'));

                    WriteLine(resultItemNote);
                }
                else
                {
                    printedItems.Add(item.Key.Substring(0, item.Key.IndexOf('|')));
                    Write(Environment.NewLine);
                    WriteLine(resultItemNote);
                }

                itemNote.Clear();
            }
        }

        /// <summary>
        /// Presents simple messages.
        /// </summary>
        /// <typeparam name="T">A generic type for dictionary values.</typeparam>
        /// <param name="message">A message to be presented.</param>
        private static void PresentSingleInfo<T>(Dictionary<string, T> message)
        {
            foreach (KeyValuePair<string, T> item in message)
            {
                StringBuilder itemNote = new StringBuilder();
                string itemDescription = item.Value.ToString();
                itemNote.Append(item.Key + ": ");
                List<string> lines = itemDescription.FormatToLines(10);
                if (lines.Count == 1)
                {
                    itemNote.Append(lines.First());
                }
                else
                {
                    itemNote.AppendLine();
                    for (int i = 0; i < lines.Count; i++)
                    {
                        if (i == lines.Count - 1)
                            itemNote.Append(lines[i]);
                        else
                            itemNote.AppendLine(lines[i]);
                    }
                }
                WriteLine(itemNote.ToString());
                itemNote.Clear();
            }
        }       

    }
}
