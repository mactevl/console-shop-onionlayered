﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;
using ConsoleShop.Presentation.Interfaces;
using ConsoleShop.Presentation.Extensions;

namespace ConsoleShop.Presentation.Types
{
    /// <summary>
    /// A public static class that represents a service to present all system manual messages in a command line.
    /// </summary>
    public static class ShopOptionsPresenter

    {
        /// <summary>
        /// Presents all messages which comes in a form of a dictionary.
        /// </summary>
        /// <typeparam name="T">A generic type for values of different fields.</typeparam>
        /// <param name="message">A message to be presented.</param>
        public static void PrintToConsole<T>(Dictionary<string, T> message)
        {
            ForegroundColor = ConsoleColor.Yellow;
            Write("============================================================> ");
            BackgroundColor = ConsoleColor.Yellow;
            ForegroundColor = ConsoleColor.Black;
            Write("OPTIONS");
            BackgroundColor = ConsoleColor.Black;
            ForegroundColor = ConsoleColor.Yellow;
            Write(" <===========================================================\n\n");

            foreach (var command in message)
            {
                string[] commandDescription = command.Value as string[];
                StringBuilder commandNote = new StringBuilder();
                commandNote.AppendLine(command.Key + ":");

                foreach (string line in commandDescription[0].FormatToLines(10))
                {
                    commandNote.AppendLine("\t" + line);
                }
                commandNote.AppendLine("\t" + "=> " + commandDescription[1]);


                Write(commandNote.ToString());
                WriteLine();
            }
            WriteLine("==================================================================================================================================");
            ForegroundColor = ConsoleColor.Cyan;
        }

        /// <summary>
        /// Presents all messages which comes in a form of a string.
        /// </summary>
        /// <param name="message">A message to be presented.</param>
        public static void PrintToConsole(string message)
        {
            ForegroundColor = ConsoleColor.Yellow;
            Write("============================================================> ");
            BackgroundColor = ConsoleColor.Yellow;
            ForegroundColor = ConsoleColor.Black;
            Write("OPTIONS");
            BackgroundColor = ConsoleColor.Black;
            ForegroundColor = ConsoleColor.Yellow;
            Write(" <===========================================================\n\n");
            
            string[] words = message.Split(" ");
            int lineLength = 5;
            int wordsCounter = 0;
            foreach(string word in words)
            {   
                Write("\t*" + word);
                wordsCounter++;
                if (wordsCounter % lineLength == 0) Write(Environment.NewLine);
                Write(Environment.NewLine);
            }
            WriteLine();
            WriteLine("==================================================================================================================================");
            ForegroundColor = ConsoleColor.Cyan;
        }
    }
}
