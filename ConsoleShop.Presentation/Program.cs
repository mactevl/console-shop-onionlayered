﻿using ConsoleShop.Presentation.Interfaces;
using ConsoleShop.Presentation.Types;
using System;
using System.Collections.Generic;

namespace ConsoleShop.Presentation
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            IApplication app = SystemSimpleFacrory.CreateApplication();
            app.Run();                     
        }


    }
}
