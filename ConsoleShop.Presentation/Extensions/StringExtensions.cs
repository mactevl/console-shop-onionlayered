﻿using ConsoleShop.Presentation.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleShop.Presentation.Extensions
{
    /// <summary>
    /// A public, static class that contains etxtension methods for strings.
    /// </summary>
    public static class StringExtensions
    {
        #region fields
        /// <summary>
        /// A set of constants which represents all field types needed by the system to perform command arguments check.
        /// </summary>
        public enum FieldTypes
        {
            /// <summary>
            /// A field of an object that represents a unique identifier.
            /// </summary>
            ID,

            /// <summary>
            /// A field of an object that represents a name.
            /// </summary>
            Name,

            /// <summary>
            /// A field of an object that represents any integer.
            /// </summary>
            Integer,

            /// <summary>
            /// A field of an object that represents user's password.
            /// </summary>
            Password,

            /// <summary>
            /// A field of an object that represents user's email.
            /// </summary>
            Email,

            /// <summary>
            /// A field of an object that represents a date.
            /// </summary>
            Date
        }
        /// <summary>
        /// A regex pattern for unique identifiers. Letters and digits only.
        /// </summary>
        private const string idPattern = @"[\p{L}\p{N}]";

        /// <summary>
        /// A regex pattern for statuses. 
        /// </summary>
        private const string statusPattern = @"^[a-zA-Z ]+$";

        /// <summary>
        /// A regex pattern for names. 
        /// </summary>
        private const string namePattern = @"[a-zA-Z]{2,}";

        /// <summary>
        /// A regex pattern for product names. 
        /// </summary>
        private const string productNamePattern = @"[^*.!@$%^&(){}[\]:;<>,.?\/~_\+\-=|]{3,}";

        /// <summary>
        /// A regex pattern for dates. DaateTime string 08/18/2018.
        /// </summary>
        private const string datePattern = @"\d{2}\/\d{2}\/\d{4}";

        /// <summary>
        /// A regex pattern for phone numers. 
        /// </summary>
        private const string phonePattern = @"^\+380\d{9}";

        /// <summary>
        /// A regex pattern for passwords. At least one: upper case letter, lower case letter, digit, special symbol, 8-20 symbols long.
        /// </summary>
        private const string passwordPattern = @"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?/~_\+\-=|]).{8,20}$";

        /// <summary>
        /// A regex pattern for emails. 
        /// </summary>
        private const string emailPattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// A regex pattern for integer values strings. 
        /// </summary>
        private const string integerPattern = @"\d+";

        /// <summary>
        /// An instance of Regex class for emails. 
        /// </summary>
        static readonly Regex emailRegex = new Regex(emailPattern);

        /// <summary>
        /// An instance of Regex class for passwords. 
        /// </summary>
        static readonly Regex passwordRegex = new Regex(passwordPattern);

        /// <summary>
        /// An instance of Regex class for unique identifiers. 
        /// </summary>
        static readonly Regex IdRegex = new Regex(idPattern);

        /// <summary>
        /// An instance of Regex class for stauses. 
        /// </summary>
        static readonly Regex statusRegex = new Regex(statusPattern);

        /// <summary>
        /// An instance of Regex class for names. 
        /// </summary>
        static readonly Regex nameRegex = new Regex(namePattern);

        /// <summary>
        /// An instance of Regex class for dates. 
        /// </summary>
        static readonly Regex dateRegex = new Regex(datePattern);

        /// <summary>
        /// An instance of Regex class for phone numbers. 
        /// </summary>
        static readonly Regex phoneRegex = new Regex(phonePattern);

        /// <summary>
        /// An instance of Regex class for product names. 
        /// </summary>
        static readonly Regex productNameRegex = new Regex(productNamePattern);

        /// <summary>
        /// An instance of Regex class for integer values strings. 
        /// </summary>
        static readonly Regex integerRegex = new Regex(integerPattern);

        /// <summary>
        /// A dictionary that maps user commands to corresponding Regex class instances needed to perform the arguments check.
        /// </summary>
        static readonly Dictionary<string, Regex[]> commandsDictionary = new Dictionary<string, Regex[]>() {
                {"searchByName", new Regex[]{ new Regex(namePattern)} },
                {"signIn", new Regex[]{ emailRegex, passwordRegex}},
                {"signUp", new Regex[]{ emailRegex, passwordRegex, passwordRegex}},
                {"addToCart", new Regex[]{ IdRegex, new Regex(integerPattern)}},
                {"changeMyInfo", new Regex[]{ nameRegex, nameRegex,
                                                         dateRegex,
                                                         phoneRegex,
                                                         emailRegex,
                                                         passwordRegex}},
                {"showClientOrders", new Regex[]{ emailRegex} },
                {"showClientInfo", new Regex[]{ emailRegex} },
                {"showAllOrders", new Regex[]{ statusRegex } },
                {"setOrderStatus", new Regex[]{ IdRegex, statusRegex}},
                {"showMyCart", new Regex[0] },
                {"clearMyCart", new Regex[0] },
                {"makeOrder", new Regex[0] },
                {"showAllProducts", new Regex[0] },
                {"man", new Regex[] { nameRegex } },
                {"signOut", new Regex[0] },
                {"showMyOrders", new Regex[0] },
                {"showMyInfo", new Regex[0]},
                {"addNewProduct", new Regex[0]},
                {"removeProduct", new Regex[]{ IdRegex } },
                {"removeFromCart", new Regex[]{ IdRegex } },
                {"editItemInCart", new Regex[]{ IdRegex, new Regex(integerPattern) } },
                {"signUpAdmin", new Regex[]{ emailRegex, passwordRegex, passwordRegex, nameRegex}},
                {"changeClientsInfo", new Regex[]{ emailRegex, nameRegex, nameRegex,
                                                                          dateRegex,
                                                                          phoneRegex,
                                                                          emailRegex
                                                                          }},
                {"changeProductInfo", new Regex[] { IdRegex, nameRegex, new Regex(productNamePattern),
                                                                        new Regex(productNamePattern),
                                                                        new Regex(productNamePattern),
                                                                        new Regex(integerPattern),
                                                                        new Regex(integerPattern)} }
            };
        #endregion fields

        /// <summary>
        /// Formats a single string message to multilined message.
        /// </summary>
        /// <param name="s">A string to be changed.</param>
        /// <param name="partLength">A length of a single line in multilined message.</param>
        /// <returns>A multilined message.</returns>
        /// <exception cref="ArgumentException"></exception>
        public static List<string> FormatToLines(this String s, Int32 partLength)
        {
            List<string> lines = new List<string>();

            if (s == null)
            {
                lines.Add("No Data!");
                return lines;
            }
                
            if (partLength <= 0)
                throw new ArgumentException("Part length has to be positive.", nameof(partLength));

            string[] words = s.Split(" ");
            StringBuilder line = new StringBuilder();
            for(int i = 0; i<words.Length; i++)
            {
                line.Append(words[i] + " ");
                if (i != 0 && i % partLength == 0 || i == words.Length - 1)
                {
                    lines.Add(line.ToString());
                    line.Clear();
                }
            }
            return lines;
        }

        /// <summary>
        /// Checks if the command provided by a user registered in the system.
        /// </summary>
        /// <param name="command">A dictionary with a command as a key and arguments as a value.</param>
        /// <returns>True if there is such a command in the system and false otherwise.</returns>
        private static bool DoesCommandExist(Dictionary<string, string[]> command) => commandsDictionary.ContainsKey(command.Keys.First());

        /// <summary>
        /// Performes validation of all commands corresponding to system's manual.
        /// </summary>
        /// <param name="command">A dictionary with a command as a key and arguments as a value.</param>
        /// <returns>True if provided command is valid and false otherwise.</returns>
        private static bool ValidateManCommand(Dictionary<string, string[]> command)
        {
            if (command.Values.First().Length > 1)
                return false;
            else if (command.Values.First().Length == 1 && !commandsDictionary[command.Keys.First()][0].IsMatch(command.Values.First()[0]))
                return true;
            else
                return true;
        }

        /// <summary>
        /// Performs validation of the changeProductInfo command.
        /// </summary>
        /// <param name="command">A dictionary with a command as a key and arguments as a value.</param>
        /// <returns>True if provided command is valid and false otherwise.</returns>
        private static bool ValidateChangeProductInfo(Dictionary<string, string[]> command)
        {
            string[] fieldNames = { "Name", "Category", "Description", "Price", "Discount" };

            if (command.Values.First().Length != 3)
                return false;

            for (int i = 0; i < 2; i++)
            {
                if (!commandsDictionary[command.Keys.First()][i].IsMatch(command.Values.First()[i]))
                    return false;
            }

            if (!fieldNames.Any(name => name.ToLower() == command.Values.First()[1].ToLower()))
                return false;

            int fieldIndex = Array.IndexOf(fieldNames, fieldNames.First(field => field.ToLower() == command.Values.First()[1].ToLower()));
            if (!commandsDictionary[command.Keys.First()][fieldIndex + 2].IsMatch(command.Values.First()[2]))
                return false;
            else
                return true;
        }

        /// <summary>
        /// Performs validation of the changeClientsInfo command.
        /// </summary>
        /// <param name="command">A dictionary with a command as a key and arguments as a value.</param>
        /// <returns>True if provided command is valid and false otherwise.</returns>
        private static bool ValidateChangeClientsInfo(Dictionary<string, string[]> command) 
        {
            if (command.Values.First().Length != 3)
                return false;

            string[] fieldNames = { "FirstName", "LastName", "BirthDate", "PhoneNumber", "Email" };

            if (!commandsDictionary[command.Keys.First()][0].IsMatch(command.Values.First()[0]))
                return false;

            if (!fieldNames.Any(name => name.ToLower() == command.Values.First()[1].ToLower()))
                return false;

            int fieldIndex = Array.IndexOf(fieldNames, fieldNames.First(field => field.ToLower() == command.Values.First()[1].ToLower()));
            if (!commandsDictionary[command.Keys.First()][fieldIndex + 1].IsMatch(command.Values.First()[2]))
                return false;
            else
                return true;
        }

        /// <summary>
        /// Performs validation of the changeMyInfo command.
        /// </summary>
        /// <param name="command">A dictionary with a command as a key and arguments as a value.</param>
        /// <returns>True if provided command is valid and false otherwise.</returns>
        private static bool ValidateChangeMyInfo(Dictionary<string, string[]> command)
        {
            if (command.Values.First().Length != 2)
                return false;

            string[] fieldNames = { "FirstName", "LastName", "BirthDate", "PhoneNumber", "Email", "Password" };

            if (!fieldNames.Any(name => name.ToLower() == command.Values.First()[0].ToLower()))
                return false;

            int fieldIndex = Array.IndexOf(fieldNames, fieldNames.First(field => field.ToLower() == command.Values.First()[0].ToLower()));
            if (!commandsDictionary[command.Keys.First()][fieldIndex].IsMatch(command.Values.First()[1]))
                return false;
            else
                return true;
        }

        /// <summary>
        /// Performs validation of all commands except those with fieldnames validation.
        /// </summary>
        /// <param name="commandString">String that represents the command.</param>
        /// <returns>True if provided command is valid and false otherwise.</returns>
        public static bool IsValidCommand(this String commandString)
        {
            Dictionary<string, string[]> command = commandString.ParseCommand();

            //is there such a command
            if (!DoesCommandExist(command)) return false;
            
            //checking a particular command
            if (command.Keys.First() == "man") return ValidateManCommand(command);
            
            if (command.Keys.First() == "changeProductInfo") return ValidateChangeProductInfo(command);

            if (command.Keys.First() == "changeClientsInfo") return ValidateChangeClientsInfo(command);

            if (command.Keys.First() == "changeMyInfo") return ValidateChangeMyInfo(command);
            
            //other commands
            if (commandsDictionary[command.Keys.First()].Length != command.Values.First().Count())
                return false;
            if (commandsDictionary[command.Keys.First()].Length > 0)
            {
                for (int i = 0; i < command.Values.First().Length; i++)
                {
                    if (!commandsDictionary[command.Keys.First()][i].IsMatch(command.Values.First()[i]))
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Removes doubled spaces from the command.
        /// </summary>
        /// <param name="stringCommand">String that represents the command.</param>
        /// <returns>The same string with corrections.</returns>
        private static string RemoveSpaceSequencesInCommand(string stringCommand)
        {
            while (stringCommand.Contains("  "))
                stringCommand = stringCommand.Replace("  ", " ");
            return stringCommand;
        }

        /// <summary>
        /// Performs a compound command parsing. The command has arguments like "canceled by user". 
        /// </summary>
        /// <param name="stringCommand">String that represents the command.</param>
        /// <returns>A command in a form of a list.</returns>
        private static List<string> ParseCompoundCommand(string stringCommand)
        {
            List<string> rusultStringCommand = new List<string>();
            StringBuilder stringBuilder = new StringBuilder();
            bool gotCompoundArgument = false;
            foreach (char chr in stringCommand)
            {
                if (!gotCompoundArgument &&
                    !chr.Equals(' ') &&
                    !chr.Equals('\"'))
                {
                    stringBuilder.Append(chr);
                    continue;
                }

                if (gotCompoundArgument &&
                         !chr.Equals('\"'))
                {
                    stringBuilder.Append(chr);
                    continue;
                }

                if (chr.Equals('\"') &&
                    !gotCompoundArgument)
                {
                    gotCompoundArgument = true;
                    continue;
                }

                if (chr.Equals('\"') &&
                   gotCompoundArgument)
                {
                    gotCompoundArgument = false;
                    rusultStringCommand.Add(stringBuilder.ToString());
                    stringBuilder.Clear();
                    continue;
                }

                if (chr.Equals(' ') && stringBuilder.Length > 0)
                {
                    rusultStringCommand.Add(stringBuilder.ToString());                    
                    stringBuilder.Clear();
                }                
            }      
            return rusultStringCommand;
        }

        /// <summary>
        /// Performs a command parsing. 
        /// </summary>
        /// <param name="stringCommand">String that represents the command.</param>
        /// <returns>A command in a form of a dictionary where the command itself is a key and arguments as a value.</returns>
        public static Dictionary<string, string[]> ParseCommand(this String stringCommand)
        {
            Dictionary<string, string[]> result = new Dictionary<string, string[]>();
            List<string> commandArray = new List<string>();


            stringCommand = RemoveSpaceSequencesInCommand(stringCommand);

            if (stringCommand.Contains('\"'))
            {
                commandArray.AddRange(ParseCompoundCommand(stringCommand));
            }
            else            
                commandArray.AddRange(stringCommand.Split(' '));            
            
            string command = commandArray[0];
            string[] commandArgs = commandArray.Skip(1).ToArray();
            result.Add(command, commandArgs);
            return result;
        }

        /// <summary>
        /// Performs regex validation for a corresponding field name.
        /// </summary>
        /// <param name="s">Field name to be checked.</param>
        /// <param name="fieldType">Corresponding type of the field.</param>
        /// <returns>True if provided field name is valid and false otherwise.</returns>
        public static bool IsValidField(this String s, FieldTypes fieldType)
        {
            Dictionary<FieldTypes, Regex> regexDictionary = new Dictionary<FieldTypes, Regex>{ { FieldTypes.ID, IdRegex}, { FieldTypes.Name, productNameRegex }, { FieldTypes.Integer, integerRegex }, { FieldTypes.Password, passwordRegex }, { FieldTypes.Email, emailRegex }, { FieldTypes.Date, dateRegex } };

            if(regexDictionary.ContainsKey(fieldType) && regexDictionary[fieldType].IsMatch(s))            
                return true;
            else
                return false;            
        }

        /// <summary>
        /// Transforms a string to Capitalized.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The capitalized string.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input[0].ToString().ToUpper() + input.Substring(1);
            }
        }

        /// <summary>
        /// Performs the check of the date to be valid.
        /// </summary>
        /// <param name="s">The date in a form of a string.</param>
        /// <returns>True if provided date is valid and false otherwise.</returns>
        public static bool IsValidDate(this string s)
        {
            //date string format: 24/08/1989
            string Date = s;

            if (Int32.Parse(Date.Substring(0, Date.IndexOf('/'))) > 12 || 
                Int32.Parse(Date.Substring(Date.IndexOf('/') + 1, 2)) > 31 || 
                Int32.Parse(Date.Substring(Date.LastIndexOf('/') + 1)) < DateTime.Parse("01/01/1922").Year ||
                Int32.Parse(Date.Substring(Date.LastIndexOf('/') + 1)) > DateTime.Now.Year)
                return false;
            else
                return true;
        }
        
    }
}
