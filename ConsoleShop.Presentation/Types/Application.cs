﻿using ConsoleShop.Domain.Types;
using System;
using System.Collections.Generic;
using System.Text;
using ConsoleShop.Domain.Interfaces;
using ConsoleShop.Presentation.Interfaces;
using static System.Console;
using System.Linq;
using ConsoleShop.Presentation.Extensions;
using System.Text.RegularExpressions;
using static ConsoleShop.Presentation.Extensions.StringExtensions;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Presentation.Types
{
    /// <summary>
    /// A public class for Main logic of the application.
    /// </summary>
    public class Application : IApplication
    {
        /// <summary>
        /// A private readonly field for SessionProxy instance.
        /// </summary>
        private readonly ISession _controller;

        /// <summary>
        /// A private property used for creation of a corresponding session through a signUp method.
        /// </summary>
        private UserStatuses UserStatus { get; set; } = UserStatuses.Client;

        /// <summary>
        /// The Application class constructor. Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="session">An instance of a Session or SessionProxy classes.</param>
        public Application(ISession session)
        {
            this._controller = session;            
        }

        /// <summary>
        /// Main application method.
        /// </summary>
        public void Run()
        {
            Title = "\"WelderPoint\". Everything you'll ever need for welding construction.";
            WindowTop = 0;
            WindowLeft = 0;
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);

            const string WELCOME_MESSAGE = "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
            + "==================================================================================>>> WELCOME to WELDERPOINT <<<=====================================================================================\n"
            + "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
            + "                                                                    ~~~ Everything you'll ever need for welding construction ~~~\n\n\n";
            ShopRespondPresenter.PrintToConsole(WELCOME_MESSAGE);
            ShopRespondPresenter.PrintToConsole("Use \"man\" for help");
            //user's commands collection
            Dictionary<string, Action<string[]>> shopOperations = new Dictionary<string, Action<string[]>>()
            {
                { "searchByName", searchByName },
                { "signIn", signIn },
                { "signUp", signUp },
                { "signUpAdmin", signUpAdmin},
                { "addToCart", addToCart},
                { "removeFromCart", removeFromCart},
                { "editItemInCart", editItemInCart},
                { "changeMyInfo", changeMyInfo},
                { "removeProduct", removeProduct},
                { "showClientOrders", showClientOrders},
                { "showClientInfo", showClientInfo},
                { "showAllOrders", showAllOrders},
                { "setOrderStatus", setOrderStatus},
                { "changeClientsInfo", changeClientsInfo},
                { "changeProductInfo", changeProductInfo},
                { "man", man}
            };
            Dictionary<string, Action> shopOperationsNoArgs = new Dictionary<string, Action>()
            {
                { "showAllProducts", showAllProducts},
                { "showMyOrders", showMyOrders},
                { "addNewProduct", addNewProduct },
                { "showMyInfo", showMyInfo},
                { "signOut", signOut },
                { "makeOrder", makeOrder},
                { "clearMyCart", clearMyCart},
                { "showMyCart", showMyCart},
            };

            Console.WriteLine("Waiting for a command: ");
            string userCommand = ReadLine().Trim();
            while (userCommand != "quit")
            {
                if (userCommand.IsValidCommand())
                {
                    var command = userCommand.ParseCommand();
                    if (command.Values.First().Length == 0 &&
                        command.Keys.First() != "man")
                    {
                        shopOperationsNoArgs[command.Keys.First()]();
                    }
                    else
                        shopOperations[command.Keys.First()](command.Values.First());
                }
                else
                {
                    ShopRespondPresenter.PrintToConsole("Command is not valid! Try again or use \"man\" command for help.");
                }
                userCommand = ReadLine().Trim();
            }
            Environment.Exit(0);
        }

        //operations under commands
        /// <summary>
        /// Gets the all information on all products presented in the shop. 
        /// </summary>
        private void showAllProducts() => ShopRespondPresenter.PrintToConsole(_controller.showAllProducts());

        /// <summary>
        /// Gets the description of all orders that had been made by the specific client.
        /// </summary>
        private void showMyOrders() => ShopRespondPresenter.PrintToConsole(_controller.showMyOrders());
            
        /// <summary>
        /// Gets the system manual.
        /// </summary>
        /// <param name="commandArgs">Whole manual if no arguments provided and a specific command entry if the command name provided as an argument.</param>
        private void man(string[] commandArgs)
        {
            if (commandArgs.Length == 0)
                ShopOptionsPresenter.PrintToConsole(_controller.man());
            else 
            {
                if (commandArgs[0].ToLower().Equals("commands"))
                {
                    Dictionary<string, string[]> resultFull = _controller.man();
                    ShopOptionsPresenter.PrintToConsole(String.Join(" ", resultFull.Keys.ToArray()));
                }
                else
                    ShopOptionsPresenter.PrintToConsole(_controller.man(commandArgs[0]));
            }
        }

        /// <summary>
        /// Registers a new administrator
        /// </summary>
        /// <param name="commandArgs">Email, password, password confirmation and the shops master key.</param>
        private void signUpAdmin(string[] commandArgs)
        {
            if(_controller.checkCredentials(commandArgs[3]))
            {
                this.UserStatus = UserStatuses.Administrator;

                signUp(new string[] { commandArgs[0], commandArgs[1], commandArgs[2] });

                this.UserStatus = UserStatuses.Client;
            }

        }

        /// <summary>
        /// Changes the information of the particular product through updating its fields.
        /// </summary>
        /// <param name="commandArgs">Product ID, field name, new value.</param>
        private void changeProductInfo(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.changeProductInfo(commandArgs[0], commandArgs[1], commandArgs[2]));

        /// <summary>
        /// Changes client's personal information.
        /// </summary>
        /// <param name="commandArgs">Email, field name, new value</param>
        private void changeClientsInfo(string[] commandArgs)
        {
            if (commandArgs[1].ToLower() == "birthdate")
            {
                string BirthDate = commandArgs[2];

                if (Int32.Parse(BirthDate.Substring(0, BirthDate.IndexOf('/'))) > 12 && Int32.Parse(BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2)) <= 12)
                {
                    string day = BirthDate.Substring(0, BirthDate.IndexOf('/'));
                    string month = BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2);
                    string year = BirthDate.Substring(BirthDate.LastIndexOf("/") + 1);
                    BirthDate = month + "/" + day + "/" + year;
                    Console.WriteLine(BirthDate);
                }

                if (!BirthDate.IsValidDate())
                {
                    ShopRespondPresenter.PrintToConsole("Wrong date format! Try again.");
                }
                else
                {
                    ShopRespondPresenter.PrintToConsole(_controller.changeClientsInfo(commandArgs[0], commandArgs[1], BirthDate));
                }
            }
            else
                ShopRespondPresenter.PrintToConsole(_controller.changeClientsInfo(commandArgs[0], commandArgs[1], commandArgs[2]));

        }

        /// <summary>
        /// Changes the status of the specific order.
        /// </summary>
        /// <param name="commandArgs">Order ID, field name, new value.</param>
        private void setOrderStatus(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.setOrderStatus(commandArgs[0], commandArgs[1].ToLower().FirstCharToUpper()));

        /// <summary>
        /// Retrieves all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="commandArgs">Order status of interest.</param>
        private void showAllOrders(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.showAllOrders(commandArgs[0].ToLower().FirstCharToUpper()));

        /// <summary>
        /// Retrieves all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="commandArgs">Client's email.</param>
        private void showClientOrders(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.showClientOrders(commandArgs[0]));

        /// <summary>
        /// Gets personal information of the particular client in the system.
        /// </summary>
        /// <param name="commandArgs">Client's email.</param>
        private void showClientInfo(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.showClientInfo(commandArgs[0]));

        /// <summary>
        /// Removes the specified product from the system.
        /// </summary>
        /// <param name="commandArgs">Product's ID.</param>
        private void removeProduct(string[] commandArgs)
        {
            ShopRespondPresenter.PrintToConsole($"Are you sure you want permanently delete the product with ID: {commandArgs[0]}? yes/no");

            string confirmation = String.Empty;
            int tries = 3;
            while(tries-- > 0 && confirmation != "yes" && confirmation != "no")
            {
                ShopRespondPresenter.PrintToConsole("Wrong input! yes/no");
                confirmation = ReadLine().Trim();                
            }

            if (confirmation == "yes")
            {
                ShopRespondPresenter.PrintToConsole(_controller.removeProduct(commandArgs[0]));
            }
            else if (confirmation == "no")
                ShopRespondPresenter.PrintToConsole("Operation has been cenceled by the client.");
            else
                ShopRespondPresenter.PrintToConsole("You had your tries. Try again in a few moments.");
        }

        /// <summary>
        /// Adds a new product to the system.
        /// </summary>
        private void addNewProduct()
        {
            Dictionary<string, string> productState = new Dictionary<string, string>();
            Dictionary<string, FieldTypes> fields = new Dictionary<string, FieldTypes>() {
                {"Name", FieldTypes.Name},
                {"Category", FieldTypes.Name},
                {"Description", FieldTypes.Name},
                {"Price", FieldTypes.Integer},
                {"Discount", FieldTypes.Integer}
            };

            string productFieldValue = String.Empty;            
            bool gotValidField = false;
            foreach (var field in fields)
            {
                int triesCount = 3;
                while (triesCount-- > 0 &&!productFieldValue.IsValidField(field.Value) && string.IsNullOrEmpty(productFieldValue))
                {
                    ShopRespondPresenter.PrintToConsole(field.Key + ": ");
                    productFieldValue = ReadLine().Trim();
                    if (productFieldValue.IsValidField(field.Value) && !string.IsNullOrEmpty(productFieldValue))
                        gotValidField = true;
                }
                if (gotValidField)
                    productState[field.Key] = productFieldValue;
                else
                {
                    ShopRespondPresenter.PrintToConsole("You had your tries. Use \"man\" for help.");
                    return;
                }
                productFieldValue = String.Empty;
            }

            ShopRespondPresenter.PrintToConsole(_controller.addNewProduct(productState));
        }

        /// <summary>
        /// Changes the date of birth of the logged-in user.
        /// </summary>
        /// <param name="commandArgs">Corresponding field name, new birth date value.</param>
        private void ChangeMyBirthDate(string[] commandArgs)
        {
             string BirthDate = commandArgs[1];
                if (Int32.Parse(BirthDate.Substring(0, BirthDate.IndexOf('/'))) > 12 && Int32.Parse(BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2)) <= 12)
                {
                    string day = BirthDate.Substring(0, BirthDate.IndexOf('/'));
                    string month = BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2);
                    string year = BirthDate.Substring(BirthDate.LastIndexOf("/") + 1);
                    BirthDate = month + "/" + day + "/" + year;
                }

                if (!BirthDate.IsValidDate())
                {
                    ShopRespondPresenter.PrintToConsole("Wrong date format! Try again.");
                }
                else
                {
                    ShopRespondPresenter.PrintToConsole(_controller.changeMyInfo(commandArgs[0], BirthDate));
                }
        }

        /// <summary>
        /// Changes the password of the logged-in user.
        /// </summary>
        /// <param name="commandArgs">Field name, new password.</param>
        private void ChangeMyPassword(string[] commandArgs) 
        {
            int triesNumber = 3;
            string confirmation = String.Empty;
            bool passwordConfirmed = false;
            while (confirmation != commandArgs[1] && triesNumber-- > 0)
            {
                ShopRespondPresenter.PrintToConsole("Plese, confirm your new password: ");
                confirmation = ReadLine().Trim();
                if (confirmation == commandArgs[1])
                    passwordConfirmed = true;
            }
            if (passwordConfirmed)
                ShopRespondPresenter.PrintToConsole(_controller.changeMyInfo(commandArgs[0], commandArgs[1]));
            else
                ShopRespondPresenter.PrintToConsole("You had your tries - come back to it in a few moments!");
        }

        /// <summary>
        /// Changes the values for the fields specified of the User type instance logged-in.
        /// </summary>
        /// <param name="commandArgs">Field name, new value</param>
        private void changeMyInfo(string[] commandArgs)
        {
            if (commandArgs[0].ToLower() == "password") ChangeMyPassword(commandArgs);
            else if (commandArgs[0].ToLower() == "birthdate") ChangeMyBirthDate(commandArgs);
            else ShopRespondPresenter.PrintToConsole(_controller.changeMyInfo(commandArgs[0], commandArgs[1]));
        }

        /// <summary>
        /// Gets all the personal information of the logged-in user.
        /// </summary>
        private void showMyInfo() => ShopRespondPresenter.PrintToConsole(_controller.showMyInfo());

        /// <summary>
        /// Creates a new order.
        /// </summary>
        private void makeOrder() => ShopRespondPresenter.PrintToConsole(_controller.makeOrder());

        /// <summary>
        /// Clears the current cart from all order items added previously.
        /// </summary>
        private void clearMyCart()
        {
            ShopRespondPresenter.PrintToConsole("Everything will be deleted from your cart! Are you sure? yes/no");
            if(ReadLine().Trim() == "yes")
                ShopRespondPresenter.PrintToConsole(_controller.clearMyCart());
            else
                ShopRespondPresenter.PrintToConsole("Operation was terminated by the client.");
        }        

        /// <summary>
        /// Gets all the information of what is in the cart currently.
        /// </summary>
        private void showMyCart() => ShopRespondPresenter.PrintToConsole(_controller.showMyCart());

        /// <summary>
        /// Edits the particular product's ammount in the cart. 
        /// </summary>
        /// <param name="commandArgs">Product ID, amount.</param>
        private void editItemInCart(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.editItemInCart(commandArgs[0], Int32.Parse(commandArgs[1])));


        /// <summary>
        /// Removes an item from the cart.
        /// </summary>
        /// <param name="commandArgs">Product ID.</param>
        private void removeFromCart(string[] commandArgs)
        {
            ShopRespondPresenter.PrintToConsole($"Are you sure you want to remove the item with ID: {commandArgs[0]} from your Cart? yes/no");
            if (ReadLine().Trim() == "yes")
            {
                ShopRespondPresenter.PrintToConsole(_controller.removeFromCart(commandArgs[0]));
            }
            else
                ShopRespondPresenter.PrintToConsole("Operation was terminated by the client.");
        }
        
        /// <summary>
        /// Adds new order item to the cart.
        /// </summary>
        /// <param name="commandArgs">Product ID, amount.</param>
        private void addToCart(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.addToCart(commandArgs[0], Int32.Parse(commandArgs[1])));

        /// <summary>
        /// Conducts sign out of the logged-in user.
        /// </summary>
        private void signOut() => ShopRespondPresenter.PrintToConsole(_controller.signOut());

        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="commandArgs">Product name.</param>
        private void searchByName(string[] commandArgs) => ShopRespondPresenter.PrintToConsole((_controller.searchByName(commandArgs[0])));

        /// <summary>
        /// Conducts sign-in of the particular client.
        /// </summary>
        /// <param name="commandArgs">Client's email, password.</param>
        private void signIn(string[] commandArgs) => ShopRespondPresenter.PrintToConsole(_controller.signIn(commandArgs[0], commandArgs[1]));

        /// <summary>
        /// Checks if a guest is adult enough to be a client.
        /// </summary>
        /// <param name="BirthDate">The date of birth (23/12/1995).</param>
        /// <returns>True if adult and false otherwise.</returns>
        private bool CheckIfAdult(string BirthDate)
        {
            TimeSpan ageControl = DateTime.Now - DateTime.Parse(BirthDate);
            if (ageControl.Days < 18 * 365)
            {
                ShopRespondPresenter.PrintToConsole("Sorry, adults only access! We have a restriction for our clients to be at least 18 years old.");
                return false;
            }
            else return true;
        }

        /// <summary>
        /// Checks if the birth date provided is valid.
        /// </summary>
        /// <param name="BirthDate">The date of birth (23/12/1995)</param>
        /// <returns>True if valid and false otherwise.</returns>
        private string SetBirthDate(string BirthDate)
        {
            Regex dateRegex = new Regex(@"\d{2}/\d{2}/\d{4}");
            int tries = 5;
            while (tries > 0 && (!dateRegex.IsMatch(BirthDate) || Int32.Parse(BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2)) > 31 || Int32.Parse(BirthDate.Substring(BirthDate.LastIndexOf('/') + 1)) < DateTime.Parse("01/01/1922").Year))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input!");
                ShopRespondPresenter.PrintToConsole("Birth Date: ");
                BirthDate = Console.ReadLine().Trim();
                tries--;
            }
            if (tries == 0 && !dateRegex.IsMatch(BirthDate))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input! The operation terminated.");
                return null;
            }

            if (Int32.Parse(BirthDate.Substring(0, BirthDate.IndexOf('/'))) > 12)
            {
                string day = BirthDate.Substring(0, BirthDate.IndexOf('/'));
                string month = BirthDate.Substring(BirthDate.IndexOf('/') + 1, 2);
                string year = BirthDate.Substring(BirthDate.LastIndexOf("/") + 1);
                BirthDate = month + "/" + day + "/" + year;
            }

            if (CheckIfAdult(BirthDate)) return BirthDate;
            else return null;
        }

        /// <summary>
        /// Checks if the name provided by the guest is valid.
        /// </summary>
        /// <param name="FirstName">New client's first name or last name.</param>
        /// <returns>True if valid and false otherwise.</returns>
        private string SetName(string FirstName)
        {
            Regex nameRegex = new Regex(@"[a-zA-Z]{2,}");
            int tries = 5;
            while (tries > 0 && !nameRegex.IsMatch(FirstName))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input!");
                ShopRespondPresenter.PrintToConsole("First Name: ");
                FirstName = Console.ReadLine().Trim();
                tries--;
            }
            if (tries == 0 && !nameRegex.IsMatch(FirstName))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input! The operation terminated.");
                return null;
            }
            else return FirstName;
        }

        /// <summary>
        /// Checks if the cellphone number provided by the guest is valid.
        /// </summary>
        /// <param name="PhoneNumber">Client's cellphone number.</param>
        /// <returns></returns>
        private string SetPhoneNumber(string PhoneNumber)
        {
            Regex phoneRegex = new Regex(@"^\+380\d{9}");
            int tries = 5;
            while (tries > 0 && !phoneRegex.IsMatch(PhoneNumber))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input!");
                ShopRespondPresenter.PrintToConsole("Phone Number: ");
                PhoneNumber = Console.ReadLine().Trim();
                tries--;
            }
            if (tries == 0 && !phoneRegex.IsMatch(PhoneNumber))
            {
                ShopRespondPresenter.PrintToConsole("Wrong Input! The operation terminated.");
                return null;
            }
            else return PhoneNumber;
        }
        
        /// <summary>
        /// Performs a sign-up operation for a new client. 
        /// </summary>
        /// <param name="commandArgs">Email, password, password confirmation.</param>
        private void signUp(string[] commandArgs)
        {
            string respond = _controller.maySignUp(commandArgs[0], commandArgs[1], commandArgs[2]);

            if (respond != "true")
                ShopRespondPresenter.PrintToConsole(respond);
            else
            {
                Dictionary<string, string> personalInfo = new Dictionary<string, string>();

                ShopRespondPresenter.PrintToConsole("Please, provide some basic information for the service to be able to proceed.");                

                ShopRespondPresenter.PrintToConsole("Birth Date(example: 08/17/2022): ");
                string BirthDate = SetBirthDate(Console.ReadLine().Trim());
                if (string.IsNullOrEmpty(BirthDate)) return;

                
                ShopRespondPresenter.PrintToConsole("First Name: ");
                string FirstName = SetName(Console.ReadLine().Trim());
                if (string.IsNullOrEmpty(FirstName)) return;

                ShopRespondPresenter.PrintToConsole("Last Name: ");
                string LastName = SetName(Console.ReadLine().Trim());
                if (string.IsNullOrEmpty(LastName)) return;

                ShopRespondPresenter.PrintToConsole("Phone Number(example: +380500894345): ");
                string PhoneNumber = SetPhoneNumber(Console.ReadLine().Trim());
                if (string.IsNullOrEmpty(PhoneNumber)) return;

                personalInfo.Add("FirstName: ", FirstName);
                personalInfo.Add("LastName: ", LastName);
                personalInfo.Add("BirthDate (example: 05/18/2022): ", BirthDate);
                personalInfo.Add("PhoneNumber: ", PhoneNumber);
                personalInfo.Add("Email: ", commandArgs[0]);
                personalInfo.Add("Password: ", commandArgs[1]);
                personalInfo.Add("User Status: ", this.UserStatus.ToString());

                ShopRespondPresenter.PrintToConsole(_controller.signUp(personalInfo));
            }
        }
        
    }
}
