﻿using ConsoleShop.Data.Interfaces;
using ConsoleShop.Data.Types;
using ConsoleShop.Domain;
using ConsoleShop.Domain.Interfaces;
using ConsoleShop.Presentation.Interfaces;
using ConsoleShop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ConsoleShop.Presentation.Types
{
    /// <summary>
    /// A public static class which bring all application's layers together.
    /// </summary>
    public static class SystemSimpleFacrory
    {
        /// <summary>
        /// Creates an Application class instance using the instance of a SessionProxy class.
        /// </summary>
        /// <returns>New Application class instance as an IApplication type.</returns>
        public static IApplication CreateApplication() => new Application(CreateSessionProxy());

        /// <summary>
        /// Creates a SessionProxy class instance using the instance of a Session class.
        /// </summary>
        /// <returns>New SessionProxy class instance as an ISession type.</returns>
        public static ISession CreateSessionProxy() => new SessionProxy(CreateSession());

        /// <summary>
        /// Creates a Session class instance using the instance of a SingletonContainer class.
        /// </summary>
        /// <returns>New Session class instance as an ISession type.</returns>
        public static ISession CreateSession() => new Session(CreateSystemRepo());

        /// <summary>
        /// Creates an instance of SingletonContainer class.
        /// </summary>
        /// <returns>An instance of SingletonContainer class.</returns>
        public static IData CreateSystemRepo() => SingletonRepoFactory.GetSystemRepo().Singleton;
    }
}
