﻿namespace ConsoleShop.Presentation.Interfaces
{
    /// <summary>
    /// An interface to be used in a SimpleFactory class.
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// Main method of the application.
        /// </summary>
        void Run();
    }
}