﻿using ConsoleShop.Data.Types;

namespace ConsoleShop.Data.Interfaces
{
    /// <summary>
    /// An interface for a SingletonContainer Class <see cref="SingletonContainer"/>.
    /// </summary>
    public interface ISingletonContainer
    {
        /// <summary>
        /// A getter for the LazyDataSingleton instance.
        /// </summary>
        LazyDataSingleton Singleton { get; }
    }
}