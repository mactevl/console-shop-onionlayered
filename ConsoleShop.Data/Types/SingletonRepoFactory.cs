﻿using ConsoleShop.Data.Interfaces;
using ConsoleShop.Data.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleShop.Domain
{
    /// <summary>
    /// A public, static class responsible for a SingletonContainer instances creation.
    /// </summary>
    public static class SingletonRepoFactory
    {
        /// <summary>
        /// A public, static method that returns a new instance of SingletonContainer class.
        /// </summary>
        /// <returns></returns>
        public static SingletonContainer GetSystemRepo() => new SingletonContainer();
    }
}
