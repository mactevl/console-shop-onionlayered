﻿using ConsoleShop.Data.Interfaces;


namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// A wrapper for the LazyDataSingleton class. The last one holds all the information collections of the system.
    /// </summary>
    public class SingletonContainer : ISingletonContainer
    {
        /// <summary>
        /// A public property with a public getter and a private setter that holds the instance of the LazyDataSingleton.
        /// </summary>
        public LazyDataSingleton Singleton { get; private set; }

        /// <summary>
        /// A constructor for the SingletonContainer class. Initializes a new instance of the <see cref="SingletonContainer"/> class, which holds the same one instance of the LazyDataSingleton.
        /// </summary>
        public SingletonContainer()
        {
            Singleton = LazyDataSingleton.Instance;
        }
    }
}
