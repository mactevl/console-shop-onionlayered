﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleShop.Data.Services;
using static ConsoleShop.Data.Types.User;
using static ConsoleShop.Data.Types.Order;
using ConsoleShop.Data.Interfaces;

namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// Lazy implementation of Singleton Class that functions like a database for the system.
    /// </summary>
    public sealed class LazyDataSingleton : IData
    {
        /// <summary>
        /// A private, static, readonly field for the Lazy Singleton instance.
        /// </summary>
        private static readonly Lazy<LazyDataSingleton> _instance = new Lazy<LazyDataSingleton>(() => new LazyDataSingleton());

        /// <summary>
        /// A propety with a public getter. Returns the Singleton instance.
        /// </summary>
        public static LazyDataSingleton Instance { get { return _instance.Value; } }

        /// <summary>
        /// Master key field.
        /// </summary>
        private readonly byte[] key = { 122, 251, 26, 65, 126, 214, 11, 168, 109, 15, 185, 151, 4, 140, 69, 174, 230, 251, 113, 14, 191, 140, 204, 86, 227, 208, 107, 121, 158, 7, 206, 116 };

        /// <summary>
        /// A property with a public getter and a private setter. Returns a dictionary of orders in the system with the client's email as a key.
        /// </summary>
        public Dictionary<string, List<Order>> ShopsOrders { get; private set; }

        /// <summary>
        ///A property with a public getter and a private setter. Returns a dictionary of products in the system with the product unique identifier as a key.
        /// </summary>
        public Dictionary<string, Product> ShopsProducts { get; private set; }

        /// <summary>
        /// A property with a public getter and a private setter. Returns a dictionary of users in the system with the client's email as a key.
        /// </summary>
        public Dictionary<string, User> ShopsUsers { get; private set; }
        
        /// <summary>
        /// A property with a public getter. Returns the total number of orders in the system.
        /// </summary>
        public int ShopsOrdersCount
        {
            get
            {
                int count = 0;
                foreach (var clientOrders in ShopsOrders)
                {
                    count += clientOrders.Value.Count;
                }
                return count;
            }
        }

        /// <summary>
        /// A public property with a getter which returns the total number of products in the system. 
        /// </summary>
        public int ShopsProductsCount { get => this.GetShopDataEntriesCount(ShopsProducts); }

        /// <summary>
        /// A public property with a getter which returns the total number of users in the system. 
        /// </summary>
        public int ShopsUsersCount { get => this.GetShopDataEntriesCount(ShopsUsers); }

        /// <summary>
        /// A property with a getter which returns the system's manual collection.
        /// </summary>
        public Dictionary<string, string[]> Manual { get; }

        /// <summary>
        /// The private LazyDataSingleton constructor. Initializes a new instance of the <see cref="LazyDataSingleton"/> class.
        /// </summary>
        private LazyDataSingleton() 
        {
            this.Manual = new Dictionary<string, string[]>() {
                {"man",
                    new string[]{"User status: Guest. Returns a list of available commands or description of a particular command.", "Example: man command. Or, to get full information, just type: man. To get list of commands: man commands"}},
                {"searchByName",
                    new string[]{"User status: Guest. Returns a list of products of a specified name.", "Example: searchByName electrodes."}},
                {"signIn",
                    new string[]{ "User status: Guest. If you are a registered user, go ahead and sign in.", "Examle: signIn welderness@gmail.com myPassword"} },                
                {"showAllProducts",
                    new string[]{ "User status: Client. Retrieves all products, presented in the shop.", "Example: showAllProducts" } },
                {"signUp",
                    new string[]{ "User status: Guest. If you are a guest, we strongly encourage you to sign up for more services. You'll need to confirm your email.", "Example: signUp welderness@gmail.com myPassword myPasswordConfirmation."} },
                {"signUpAdmin",
                    new string[]{ "User status: Administrator. A person who has the shop's master key may register a new administrator.", "Example: signUpAdmin welderness@gmail.com myPassword myPasswordConfirmation masterKey."} },
                {"signOut",
                    new string[]{ "User status: Guest. To leave your cabinet just type signOut.", "Example: signOut"} },
                {"addToCart",
                    new string[]{ "User status: Client. Adds new order item to your cart.", "Example: addToCart productID amount" } },
                {"editItemInCart",
                    new string[]{ "User status: Client. Corrects the amount of a particular product in your cart.", "Example: editItemInCart productID amount" } },
                {"removeFromCart",
                    new string[]{ "User status: Client. Removes a particular product from your cart.", "Example: removeFromCart productID" } },
                {"showMyCart",
                    new string[]{ "User status: Client. Retrieves data on all products that are currently in your cart.", "Example: showMyCart" } },
                {"showMyOrders",
                    new string[]{ "User status: Client. Retrieves data on all orders that has been made by the clien.", "Example: showMyOrders" } },
                {"clearMyCart",
                    new string[]{ "User status: Client. Removes all products that are currently in your cart.", "Example: clearMyCart" } },
                 {"makeOrder",
                    new string[]{ "User status: Client. Creates a new order from products that are currently in your cart.", "Example: makeOrder" } },
                 {"showMyInfo",
                    new string[]{ "User status: Client. Retrieves all your personal data that the system retains.", "Example: showMyInfo" } },
                 {"changeMyInfo",
                    new string[]{ "User status: Client. Anables you to make changes in the account updating your personal data.", "Example: changeMyInfo fieldName value" } },
                 {"addNewProduct",
                    new string[]{ "User status: Administrator. Adds new product to the system. Prices should be of type integer.", "Example: addNewProduct" } },
                 {"removeProduct",
                    new string[]{ "User status: Administrator. Removes the particular product from the system.", "Example: removeProduct productID" } },
                 {"showClientOrders",
                    new string[]{ "User status: Administrator. Retrives information on all orders made by the particular user.", "Example: showClientOrders email" } },
                 {"showClientInfo",
                    new string[]{ "User status: Administrator. Retrieves personal information of the particular client.", "Example: showClientInfo email" } },
                 {"showAllOrders",
                    new string[]{ "User status: Administrator. Retrives information on all orders with the status provided. Statuses: \"New\", \"PaymentReceived\", \"Sent\", \"CanceledByUser\", \"CanceledByAdministrator\", \"Received\", \"Completed\"", "Example: showAllOrders Sent" } },
                 {"setOrderStatus",
                    new string[]{ "User status: Administrator. Changes a status of the particular order. Statuses: \"New\", \"PaymentReceived\", \"Sent\", \"CanceledByUser\", \"CanceledByAdministrator\", \"Received\", \"Completed\"", "Example: setOrderStatus orderID orderStatus" } },
                 {"changeClientsInfo",
                    new string[]{ "User status: Administrator. Anables Administrators to edit client's personal information.", "Example: changeClientsInfo  email fieldName value" } },
                 {"changeProductInfo",
                    new string[]{ "User status: Administrator. Anables Administrators to edit details of the particular product in the system. Prices should be of type integer.", "Example: changeProductInfo productID fieldName value" } }
            };

            this.ShopsProducts = new Dictionary<string, Product>{
                { "product_1", new Product(productID: "product_1",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "15 X 2.5 (1/2\"): Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 4560, discount: 0) },
                { "product_2", new Product(productID: "product_2",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "20 X 2.5 (3/4\"): Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 5790, discount: 0)},
                { "product_3", new Product(productID: "product_3",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "25 X 2.8 (1\"): Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 8110, discount: 0)},
                { "product_4", new Product(productID: "product_4",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "40 X 3.0 (1 1/2\"): Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 12380, discount: 0)},
                { "product_5", new Product(productID: "product_5",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "50 X 3.0 (2\"): Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 16470, discount: 0)},
                { "product_6", new Product(productID: "product_6",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "57 X 2.8: Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 13490, discount: 0)},
                { "product_7", new Product(productID: "product_7",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "76 X 3.0: Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 21030, discount: 0)},
                { "product_8", new Product(productID: "product_8",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "108 X 3.0: Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 29890, discount: 0)},
                { "product_9", new Product(productID: "product_9",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "127 X 4.0: Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 27340, discount: 0)},
                { "pipe1X", new Product(productID: "pipe1X",
                                  name: "Steel Round Pipe",
                                  category: "Pipes",
                                  description: "159 X 4.0: Steel water and gas pipe ASME/ANSI B36.10.",
                                  price: 53500, discount: 0)},
                { "pipe2X", new Product(productID: "pipe2X",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "15 X 15 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 27100, discount: 5)},
                { "pipe3X", new Product(productID: "pipe3X",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "20 X 20 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 4000, discount: 5)},
                { "product_13", new Product(productID: "product_13",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "25 X 25 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 5040, discount: 5)},
                { "product_14", new Product(productID: "product_14",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "20 X 30 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 4710, discount: 5)},
                { "product_15", new Product(productID: "product_15",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "30 X 30 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 5580, discount: 5)},
                { "product_16", new Product(productID: "product_16",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "40 X 20 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 6160, discount: 5)},
                { "product_17", new Product(productID: "product_17",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "40 X 25 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 6700, discount: 5)},
                { "product_18", new Product(productID: "product_18",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "40 X 40 X 3.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 8200, discount: 5)},
                { "product_19", new Product(productID: "product_19",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "50 X 25 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 7780, discount: 5)},
                { "product_20", new Product(productID: "product_20",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "50 X 30 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 8200, discount: 5)},
                { "product_21", new Product(productID: "product_21",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "50 X 50 X 3.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 15850, discount: 5)},
                { "product_22", new Product(productID: "product_22",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "60 X 30 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 9440, discount: 5)},
                { "product_23", new Product(productID: "product_23",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "60 X 40 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 10550, discount: 5)},
                { "product_24", new Product(productID: "product_24",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "60 X 40 X 3.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 15440, discount: 5)},
                { "product_25", new Product(productID: "product_25",
                                  name: "Steel Profile Pipe",
                                  category: "Pipes",
                                  description: "60 X 60 X 2.0: Steel square profile pipe DIN 2395. Square metal pipe is a rolled metal product produced for mass use. It meets the standard of DIN 2395. The measured length of the product must be within the range of 5-9 meters, products of unmeasured length are sold within the same range.",
                                  price: 13000, discount: 5)},
                { "product_26", new Product(productID: "product_26",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "32 X 32: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 4850, discount: 2)},
                { "product_27", new Product(productID: "product_27",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "35 X 35: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 5080, discount: 2)},
                { "product_28", new Product(productID: "product_28",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "40 X 40 X 3.0: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 5960, discount: 2)},
                { "product_29", new Product(productID: "product_29",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "45 X 45 X 4.0: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 8800, discount: 2)},
                { "product_30", new Product(productID: "product_30",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "50 X 50 X 4.0: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 10200, discount: 2)},
                { "product_31", new Product(productID: "product_31",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "63 X 63: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 15700, discount: 2)},
                { "product_32", new Product(productID: "product_32",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "75 X 75 X 5.0: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 18830, discount: 2)},
                { "product_33", new Product(productID: "product_33",
                                  name: "Steel Angle Bar",
                                  category: "Bars",
                                  description: "90 X 90 X 6.0: Steel equal leg angle bar DIN 1028–94\r\nSteel equal leg angle bar is a product classified as rolled steel and made of high-quality constructional steel. Hot-rolled equal leg angle bar is used in all spheres of industry. Most often, it is in demand in the construction sector in the form of rigid reinforcement, when it is necessary to reinforce concrete in monolithic structures of high-rise frames.",
                                  price: 27920, discount: 2)},
                { "product_34", new Product(productID: "product_34",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "6.5: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 20800, discount: 3)},
                { "product_35", new Product(productID: "product_35",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "8: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 22900, discount: 3)},
                { "product_36", new Product(productID: "product_36",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "12: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 34850, discount: 3)},
                { "product_37", new Product(productID: "product_37",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "14: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 53250, discount: 3)},
                { "product_38", new Product(productID: "product_38",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "16: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 62700, discount: 3)},
                { "product_39", new Product(productID: "product_39",
                                  name: "Hot-Rolled Steel Channel",
                                  category: "Channel",
                                  description: "18: Hot-rolled steel channel DIN 1026. The hot-rolled steel channel meets the acquirements of DIN 1026. Carbon steel of which the channel is made cannot be deformed and has a long-term service life. Hot-rolled channel GOST 8240-89 is designed to increase the strength of structures.",
                                  price: 73700, discount: 3)},
                { "product_40", new Product(productID: "product_40",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "8: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 1210, discount: 0)},
                { "product_41", new Product(productID: "product_41",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "10: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 1750, discount: 0)},
                { "product_42", new Product(productID: "product_42",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "12: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 2510, discount: 0)},
                { "product_43", new Product(productID: "product_43",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "14: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 3520, discount: 0)},
                { "product_44", new Product(productID: "product_44",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "16: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 4550, discount: 0)},
                { "product_45", new Product(productID: "product_45",
                                  name: "Reinforcement Bar",
                                  category: "Bar",
                                  description: "18: Reinforcement bar ASTM A615/A615M-08a. It is used as a reinforcing or integral element in various types of building structures. It is made of carbon steels by hot-rolled method of manufacture. The section of reinforcement bar can be periodic or round.",
                                  price: 5760, discount: 0)},
                { "product_46", new Product(productID: "product_46",
                                  name: "Hot-Rolled Steel Flat Bar",
                                  category: "Flat Bar",
                                  description: "25 X 4.0: Hot-rolled steel flat bar EN10058. Hot rolled flat bar is a fairly widespread product of a rectangular metal-roll. Flat bars are mainly subjected to stress during compression and twisting. Therefore, special requirements for chemical composition are imposed on steel from which they are rolled. It must strictly comply with EN10058.",
                                  price: 2540, discount: 0)},
                { "product_47", new Product(productID: "product_47",
                                  name: "Hot-Rolled Steel Flat Bar",
                                  category: "Flat Bar",
                                  description: "30 X 4.0: Hot-rolled steel flat bar EN10058. Hot rolled flat bar is a fairly widespread product of a rectangular metal-roll. Flat bars are mainly subjected to stress during compression and twisting. Therefore, special requirements for chemical composition are imposed on steel from which they are rolled. It must strictly comply with EN10058.",
                                  price: 3000, discount: 0)},
                { "product_48", new Product(productID: "product_48",
                                  name: "Hot-Rolled Steel Flat Bar",
                                  category: "Flat Bar",
                                  description: "40 X 4.0: Hot-rolled steel flat bar EN10058. Hot rolled flat bar is a fairly widespread product of a rectangular metal-roll. Flat bars are mainly subjected to stress during compression and twisting. Therefore, special requirements for chemical composition are imposed on steel from which they are rolled. It must strictly comply with EN10058.",
                                  price: 4050, discount: 0)},
                { "product_49", new Product(productID: "product_49",
                                  name: "Hot-Rolled Steel Flat Bar",
                                  category: "Flat Bar",
                                  description: "50 X 4.0: Hot-rolled steel flat bar EN10058. Hot rolled flat bar is a fairly widespread product of a rectangular metal-roll. Flat bars are mainly subjected to stress during compression and twisting. Therefore, special requirements for chemical composition are imposed on steel from which they are rolled. It must strictly comply with EN10058.",
                                  price: 5030, discount: 0)},
                { "product_50", new Product(productID: "product_50",
                                  name: "Hot-Rolled Steel Flat Bar",
                                  category: "Flat Bar",
                                  description: "50 X 5.0: Hot-rolled steel flat bar EN10058. Hot rolled flat bar is a fairly widespread product of a rectangular metal-roll. Flat bars are mainly subjected to stress during compression and twisting. Therefore, special requirements for chemical composition are imposed on steel from which they are rolled. It must strictly comply with EN10058.",
                                  price: 6320, discount: 0)},
                { "product_51", new Product(productID: "product_51",
                                  name: "Electrodes Monolith 3mm/2.5kg",
                                  category: "Electrodes",
                                  description: "MMA ARC Welding Electrodes Rod STICK Mild E6012 3.0mm 350mm 2.5kg Rutile",
                                  price: 15000, discount: 0)},
                { "product_52", new Product(productID: "product_52",
                                  name: "Electrodes MAXweld 3mm/2.5kg",
                                  category: "Electrodes",
                                  description: "MMA ARC Welding Electrodes Grey STICK Mild E6012 3.0mm 350mm 2.5kg Rutile",
                                  price: 17500, discount: 0)},
                { "product_53", new Product(productID: "product_53",
                                  name: "Electrodes Paton 3mm/2.5kg",
                                  category: "Electrodes",
                                  description: "MMA ARC Welding Electrodes Green STICK Mild E6012 3.0mm 350mm 2.5kg Rutile",
                                  price: 16000, discount: 0)},
                { "product_54", new Product(productID: "product_54",
                                  name: "Steel Sheet",
                                  category: "Sheet",
                                  description: "1 X 2 X 3.0: Hot-rolled steel sheet ASTM A568/A568M. ASTM of this kind of sheets is applied to hot-rolled steel; its width is more than 500 mm. Hot-rolled sheet is a metal-rolled sheet of carbon steel of high or ordinary quality.",
                                  price: 184900, discount: 0)},
                { "product_55", new Product(productID: "product_55",
                                  name: "Steel Sheet",
                                  category: "Sheet",
                                  description: "1 X 2 X 1.2: Hot-rolled steel sheet ASTM A568/A568M. ASTM of this kind of sheets is applied to hot-rolled steel; its width is more than 500 mm. Hot-rolled sheet is a metal-rolled sheet of carbon steel of high or ordinary quality.",
                                  price: 79000, discount: 0)},

            };
   
            this.ShopsUsers = new Dictionary<string, User>
            {
                { "akiva_goldsman@yahoo.com", new User(userID: "user1X", 
                                            userStatus: UserStatuses.Client, 
                                            firstName: "Akiva", 
                                            lastName: "Goldsman", 
                                            birthDate: new DateTime(1962, 7, 7).Date, 
                                            phoneNumber: "+1 757-428-0583", 
                                            email: "akiva_goldsman@yahoo.com", 
                                            password: "Gold_Ak_62")},
                { "jeff_vintar@gmail.com", new User(userID: "user2X",
                                            userStatus: UserStatuses.Client,
                                            firstName: "Jeff",
                                            lastName: "Vintar",
                                            birthDate: new DateTime(1964, 5, 2).Date,
                                            phoneNumber: "+1 757-422-4321",
                                            email: "jeff_vintar@gmail.com",
                                            password: "ViN_#6890")},
                { "chi_mcbride@aol.com", new User(userID:"user3X",
                                            userStatus: UserStatuses.Client,
                                            firstName: "Chi",
                                            lastName: "McBride",
                                            birthDate: new DateTime(1961, 9, 23).Date,
                                            phoneNumber: "+1 757-428-1088",
                                            email: "chi_mcbride@aol.com",
                                            password: "BrIghT_61_23")},
                {  "will_smith@aol.com", new User(userID:"user1X",
                                            userStatus: UserStatuses.Administrator,
                                            firstName: "Will",
                                            lastName: "Smith",
                                            birthDate: new DateTime(1968, 9, 25).Date,
                                            phoneNumber: "+1 757-428-1431",
                                            email: "will_smith@aol.com",
                                            password: "$IRoBot_68_25")},
            };

            this.ShopsOrders = new Dictionary<string, List<Order>>
            {
                { "will_smith@aol.com", new List<Order>() {new Order(orderId:"order1X",
                                        clientId: "user1X",
                                        orderDate: new DateTime(2022, 7, 15),
                                        orderTotalSum: 53500,
                                        orderStatus: OrderStatuses.Received,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe1X"], 10)}), 
                                        new Order(orderId:"order2X",
                                        clientId: "user1X",
                                        orderDate: new DateTime(2022, 7, 15),
                                        orderTotalSum: 53500,
                                        orderStatus: OrderStatuses.CanceledByUser,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe1X"], 10)}),
                                        new Order(orderId:"order3X",
                                        clientId: "user1X",
                                        orderDate: new DateTime(2022, 7, 15),
                                        orderTotalSum: 53500,
                                        orderStatus: OrderStatuses.CanceledByAdministrator,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe1X"], 10)})}},

                { "chi_mcbride@aol.com", new List<Order>() {new Order(orderId:"order4X",
                                        clientId: "user3X",
                                        orderDate: new DateTime(2022, 9, 8),
                                        orderTotalSum: 27100,
                                        orderStatus: OrderStatuses.New,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe2X"], 10)}),
                                        new Order(orderId:"order5X",
                                        clientId: "user3X",
                                        orderDate: new DateTime(2022, 9, 8),
                                        orderTotalSum: 27100,
                                        orderStatus: OrderStatuses.New,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe2X"], 10)}),
                                        new Order(orderId:"order6X",
                                        clientId: "user3X",
                                        orderDate: new DateTime(2022, 9, 8),
                                        orderTotalSum: 27100,
                                        orderStatus: OrderStatuses.Completed,
                                        orderItems: new List<OrderItem>(){ new OrderItem(ShopsProducts["pipe2X"], 10)})}},
            };
        }

        /// <summary>
        /// Checks the master key provided within the process of registration of a new administrator. 
        /// </summary>
        /// <param name="masterPassword">The shop's master key.</param>
        /// <returns>True if the provided password identical to the system's one, and false otherwise.</returns>
        public bool MasterKey(string masterPassword) => PasswordHasher.GetHash(masterPassword).SequenceEqual(key);

        /// <summary>
        /// Adds a new order to the system.
        /// </summary>
        /// <param name="email">An email of the client.</param>
        /// <param name="order">The new instance of the Order class that should be added to the system.</param>
        /// <returns>True if the order had been added to the system, and false otherwise.</returns>
        public bool AddOrder(string email, Order order)
        {
            int usersOrdersCount = 0;
            if (this.ShopsOrders.ContainsKey(email))
            {
                usersOrdersCount = this.ShopsOrders[email].Count;
                this.ShopsOrders[email].Add(order);
            }
            else
                this.ShopsOrders.Add(email, new List<Order> { order });

            if (this.ShopsOrders[email].Count != usersOrdersCount)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks if the system possesses the specified product.
        /// </summary>
        /// <param name="productId">The unique product's identifier.</param>
        /// <returns>True if thehe is the product in the system, and false otherwise.</returns>
        public bool IsInProducts(string productId) => this.ShopsProducts.ContainsKey(productId);

        /// <summary>
        /// Conducts a search of the specified product by its unique identifier in the products' collection.
        /// </summary>
        /// <param name="productId">The product's unique identifier.</param>
        /// <returns>The corresponding instance of the Product type.</returns>
        public Product searchByProductId(string productId)
        {
            if (this.ShopsProducts.ContainsKey(productId)) 
                return this.ShopsProducts[productId];
            else return null;
        }

        /// <summary>
        /// A generic method which counts entries in main collections of the type(users, products, orders).
        /// </summary>
        /// <typeparam name="T">Generic type for values of main dictionaries.</typeparam>
        /// <param name="entriesCollection">A reference to the dictionary, where key-value pairs should be counted.</param>
        /// <returns>The number of entries in the specified dictionary.</returns>
        public int GetShopDataEntriesCount<T>(Dictionary<string, T> entriesCollection) => entriesCollection.Count;

        //IGuestData Interface
        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="productName">The name of the product of interest.</param>
        /// <returns>All the information on the products that had been found in the system.</returns>
        public Dictionary<string, string> searchByName(string productName)
        {
            Product[] productsList = ShopsProducts.Values.ToArray();

            var result = new Dictionary<string, string>();


            foreach(Product product in productsList)
            {
                if(product.Name.ToLower() == productName.ToLower() || product.Name.ToLower().Contains(productName.ToLower()))
                {
                    foreach(var productField in product.GetInfo())
                        result.Add($"ProductID: {product.ID} | " + productField.Key, productField.Value);
                }
            }

            if (result.Count > 0) return result;
            else
            {
                result.Add("Result: ", "No results were found that match your request!");
                return result;
            }
        }

        /// <summary>
        /// Gets the all information on all products presented in the shop. 
        /// </summary>
        /// <returns>The information on all products in the shop in the form of a dictionary.</returns>
        public Dictionary<string, string> showAllProducts()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            Product[] productsList = ShopsProducts.Values.ToArray();

            foreach (Product product in productsList)
            {
                foreach (var productField in product.GetInfo())
                    result.Add($"ProductID: {product.ID} | " + productField.Key, productField.Value);
            }
            return result;
        }

        /// <summary>
        /// Gets the description of all orders that had been made by the specific client.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The information in the form of a dictionary on all orders that had been made by the client.</returns>
        public Dictionary<string, string> showMyOrders(string email)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            if (ShopsOrders.ContainsKey(email))
            {
                List<Order> ordersList = ShopsOrders[email];

                foreach(Order order in ordersList)
                {
                    foreach(var orderInfo in order.GetInfo())
                        result.Add($"Order ID: {order.OrderId} | " + orderInfo.Key, orderInfo.Value);
                }

                return result;
            }
            else
            {
                result.Add("Result: ", "You have no orders for now!");
                return result;
            }
        }

        /// <summary>
        /// Changes the status of the specific order.
        /// </summary>
        /// <param name="orderID">Order's unique identifier.</param>
        /// <param name="status">The status to be set.</param>
        /// <returns>The updated information on the specific order.</returns>
        public Dictionary<string, string> setOrderStatus(string orderID, OrderStatuses status)
        {
            foreach(List<Order> clientsOrders in ShopsOrders.Values)
            {
                foreach (Order order in clientsOrders)
                {
                    if(order.OrderId == orderID)
                    {
                        order.SetStatus(status);
                        return order.GetInfo();
                    }                   

                }
            }

            return new Dictionary<string, string>() { { "Result: ", "There is no such an order in the system!" } };            
        }

        /// <summary>
        /// Gets all the personal information that the system has on the specific client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>The information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showMyInfo(string email) => ShopsUsers[email].GetInfo();

        /// <summary>
        /// Changes the values for the fields specified of the User type instance.
        /// </summary>
        /// <typeparam name="T">A generic type for values of User type instance fields.</typeparam>
        /// <param name="email">The client's email.</param>
        /// <param name="fieldName">The field name that should be changed.</param>
        /// <param name="value">A new value for the field.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeMyInfo<T>(string email, string fieldName, T value)
        {
            ShopsUsers[email].EditInfo(fieldName, value);
            if (fieldName.Equals("Email"))
            {
                ShopsUsers[value as String] = ShopsUsers[email];
                ShopsUsers.Remove(email);
                return ShopsUsers[value as String].GetInfo();
            }
            return ShopsUsers[email].GetInfo();
        }

        //IAdministratorInterface
        /// <summary>
        /// Gets personal information of the particular client in the system.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The client's personal information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showClientInfo(string email) => ShopsUsers[email].GetInfo();

        /// <summary>
        /// Changes the state of the User type instance through updating its fields with new values provided.
        /// </summary>
        /// <typeparam name="T">A generic type for values of User type instance fields.</typeparam>
        /// <param name="email">Client's email.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeClientInfo<T>(string email, string fieldName, T value) => changeMyInfo(email, fieldName, value);


        /// <summary>
        /// Changes the information of the particular product through updating its fields.
        /// </summary>
        /// <typeparam name="T">A generic type for values of Product type instance fields.</typeparam>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific product.</returns>
        public Dictionary<string, string> changeProductInfo<T>(string productID, string fieldName, T value)
        {
            if (ShopsProducts.ContainsKey(productID))
            {
                ShopsProducts[productID].EditInfo(fieldName, value);
                return ShopsProducts[productID].GetInfo();
            }
            else
            {
                return new Dictionary<string, string>() { { "Result: ", "There is no such a product in the system!" } };
            }
        }

        /// <summary>
        /// Removes the specified product from the system.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <returns>True if the product had been deleted successfully, and false otherwise. </returns>
        public bool removeProduct(string productID) => this.ShopsProducts.Remove(productID);

        /// <summary>
        /// Checks if the client has any orders.
        /// </summary>
        /// <param name="email">An email of the client.</param>
        /// <returns>True if the client has any orders, and false otherwise.</returns>
        public bool HasOrders(string email) => this.ShopsOrders.ContainsKey(email);

        /// <summary>
        /// Moves orders in the ShopsOrders dictionary under the updated email.
        /// </summary>
        /// <param name="oldShopsOrdersEmailKey">Old user's email</param>
        /// <param name="newShopsOrdersEmailKey">New user's email</param>
        public void MoveOrders(string oldShopsOrdersEmailKey, string newShopsOrdersEmailKey)
        {
            this.ShopsOrders[newShopsOrdersEmailKey] = this.ShopsOrders[oldShopsOrdersEmailKey];
            this.ShopsOrders.Remove(oldShopsOrdersEmailKey);
        }

        /// <summary>
        /// Retrieves all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="status">the order's status.</param>
        /// <returns>All the information on all orders which have the particular status set.</returns>
        public Dictionary<string, string> showAllOrders(OrderStatuses status)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            List<Order> ordersList = new List<Order>();

            foreach(var clientOrders in ShopsOrders)
            {
                clientOrders.Value.Where(order => order.OrderStatus == status).ToList().ForEach(order => ordersList.Add(order));
            }

            if (ordersList.Count == 0)
            {
                result.Add("Result: ", $"There are no orders with status \"{status.ToString()}\" in the system!");
                return result;
            }
            int ordersCounter = 0;
            foreach(Order order in ordersList)
            {
                ordersCounter++;
                foreach (var orderInfoProp in order.GetInfo())
                    result.Add($"Order {ordersCounter}:\nOrder ID: {order.OrderId}" + " | " + orderInfoProp.Key, orderInfoProp.Value);
            }

            return result;
        }

        /// <summary>
        ///  Retrieves all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>All the information on all orders which correspond to the particular client.</returns>
        public Dictionary<string, string> showClientOrders(string email)
        {
            if(ShopsOrders.ContainsKey(email))
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                foreach(Order clientOrder in ShopsOrders[email]) 
                {
                    foreach(var clientOrderProp in clientOrder.GetInfo())
                        result.Add($"Order ID: {clientOrder.OrderId}" + " | " + clientOrderProp.Key, clientOrderProp.Value);
                   
                }
                return result;
            }
            else
            {
                return new Dictionary<string, string>() { { "Result: ", $"The client with email: {email} has no orders yet!" } };
            }
        }

        /// <summary>
        /// Checks if there is a client had already been registered in the system with specified email. 
        /// </summary>
        /// <param name="email">An email of the guest or a shop's client.</param>
        /// <returns>True if there is a client in the system with the email specified.</returns>
        public bool IsClient(string email) => this.ShopsUsers.ContainsKey(email);

        /// <summary>
        /// Gets the instance of the type User corresponding to the email specified.
        /// </summary>
        /// <param name="email">An email of a shop's client.</param>
        /// <returns>The instance of the type User corresponding to the email specified.</returns>
        public User GetClient(string email) => this.ShopsUsers[email];

        /// <summary>
        /// Adds a new client to the system.
        /// </summary>
        /// <param name="email">An email of a guest.</param>
        /// <param name="user">The new instance of the User class that should be added to the system.</param>
        /// <returns>Confirmation or warning string</returns>
        public string AddClient(string email, User user)
        {
            if (!ShopsUsers.ContainsKey(email))
            {
                this.ShopsUsers.Add(email, user);
                return "We are glad to welcome You in our family! Feel free to explore everything in here.";
            }                
            else
                return "Something went wrong! We can't register you now. Please, try again in a moment.";
        }

        /// <summary>
        /// Adds new product to the system.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="newProduct">The new instance of a Product type that should be added to the system.</param>
        /// <returns>True if the product had been added to the system, and false otherwise.</returns>
        public bool AddProduct(string productID, Product newProduct)
        {
            this.ShopsProducts.Add(productID, newProduct);
            if (this.ShopsProducts.ContainsKey(productID))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Adds a new product to the system.
        /// </summary>
        /// <param name="name">The new product's name.</param>
        /// <param name="category">The new product's category.</param>
        /// <param name="description">The new product's description.</param>
        /// <param name="price">The new product's price.</param>
        /// <param name="discount">The new product's discount.</param>
        /// <returns>All the information on the new product.</returns>
        public Dictionary<string, string> AddProduct(string name, string category, string description, int price, int discount)
        {
            int shopProductsCount = ShopsProducts.Count;
            Product newProduct = new Product(IDsHasher.GetID(shopProductsCount + 1),
                                             name: name,
                                             category: category,
                                             description: description,
                                             price: price,
                                             discount: discount);

            ShopsProducts.Add(newProduct.ID, newProduct);
            return newProduct.GetInfo();
        }

        /// <summary>
        /// Gets the unique identifier of the client who made the order.
        /// </summary>
        /// <param name="orderID">The unique identifier of the order.</param>
        /// <returns>The unique identifier of the client who made the order.</returns>
        public string GetOwnerId(string orderID)
        {
            List<Order> allOrders = new List<Order>();
            this.ShopsOrders.Values.ToList().ForEach(list => allOrders.AddRange(list));
            return allOrders.First(order => order.OrderId.Equals(orderID)).ClientId;

        }

        /// <summary>
        /// Checks if there is the order with the specified unique identifier in the system. 
        /// </summary>
        /// <param name="orderID">The order's specified unique identifier.</param>
        /// <returns>True if the order had already been added to the system, and false otherwise.</returns>
        public bool InOrders(string orderID)
        {
            List<Order> allOrders = new List<Order>();
            this.ShopsOrders.Values.ToList().ForEach(list => allOrders.AddRange(list));
            return allOrders.Any(order => order.OrderId.Equals(orderID));
        }

    }
}
