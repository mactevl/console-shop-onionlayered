﻿using ConsoleShop.Data.Types;
using ConsoleShop.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ConsoleShop.Data.Types.Order;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Services
{
    /// <summary>
    /// SessionProxy class bears credentials check, logging, messaging functionalities etc.
    /// </summary>
    public class SessionProxy : ISession
    {
        /// <summary>
        /// Private readonly field which holds the reference to the system's session.
        /// </summary>
        private readonly ISession _session;

        /// <summary>
        /// Warning message to be shown when users are trying to execute commands with the higher status.
        /// </summary>
        private readonly Dictionary<string, string> warningMessage = new Dictionary<string, string>() { { "Warning! ", "You do not have permissions to conduct the operation." } };
        private readonly string[] clientOrderStatusesStrings = { "CanceledByUser", "Received", "Canceled by user" };

        /// <summary>
        /// A collection of all valid forms of order statuses that can be passed to the session by a client. 
        /// </summary>
        private readonly string[] adminOrderStatusesStrings = { "New", "PaymentReceived", "Sent", "CanceledByAdministrator", "Completed", "Payment received", "Canceled by administrator" };

        /// <summary>
        /// A public getter for a logged-in user.
        /// </summary>
        public User GetLoggedInUser => _session.GetLoggedInUser;

        /// <summary>
        /// The SessionProxy object's constructor. Initializes a new instance of the <see cref="SessionProxy"/> class.
        /// </summary>
        /// <param name="session">A reference to the system's session object.</param>
        public SessionProxy(ISession session)
        {
            _session = session;
        }

        /// <summary>
        /// Checks if the shop's user may add a new product to the system.
        /// </summary>
        /// <param name="productState">A dictionary with all the data needed for a new product registration.</param>
        /// <returns>All the information on the new product.</returns>
        public Dictionary<string, string> addNewProduct(Dictionary<string, string> productState)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.addNewProduct(productState);
            else
                return warningMessage;
        }


        /// <summary>
        /// Checks if the shop's user may add a new product to the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string addToCart(string productID, int amount)
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.addToCart(productID, amount);
            else
                return $"{warningMessage.Keys.First()}{warningMessage.Values.First()}";
        }

        /// <summary>
        /// Checks if the shop's user is allowed to make changes to the state of the User type instance through updating its fields with new values provided.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeClientsInfo(string email, string fieldName, string value)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.changeClientsInfo(email, fieldName, value);
            else
                return warningMessage;
        }

        /// <summary>
        ///  Checks if the shop's user is allowed to make changes to the values for the fields specified of the User type instance in the current session.
        /// </summary>
        /// <param name="fieldName">The field name that should be changed.</param>
        /// <param name="value">A new value for the field.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeMyInfo(string fieldName, string value)
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.changeMyInfo(fieldName, value);
            else
                return warningMessage;
        }

        /// <summary>
        ///  Checks if the shop's user is allowed to make changes to the information of the particular product through updating its fields.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific product.</returns>
        public Dictionary<string, string> changeProductInfo(string productID, string fieldName, string value)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.changeProductInfo(productID, fieldName, value);
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if a new administrator registration attempt is valid.
        /// </summary>
        /// <param name="masterPassword">Shop's master key, provided by the shop's user.</param>
        /// <returns>True if the master key is correct, and false otherwise.</returns>
        public bool checkCredentials(string masterPassword) => _session.checkCredentials(masterPassword);

        /// <summary>
        /// Checks if the shop's user is allowed to remove all order items from the cart.
        /// </summary>
        /// <returns>A confirmation or warning string.</returns>
        public string clearMyCart()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.clearMyCart();
            else
                return $"{warningMessage.Keys.First()}{warningMessage.Values.First()}";
        }

        /// <summary>
        /// Checks if the shop's user is allowed to Edit the amount of the particular product in the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>Updated data on the current cart.</returns>
        public Dictionary<string, string> editItemInCart(string productID, int amount)
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.editItemInCart(productID, amount);
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the session status equals to the status needed to execute the particular command.
        /// </summary>
        /// <param name="userStatus">Status needed to execute the particular command.</param>
        /// <returns>True if the session status is enough, and false otherwise.</returns>
        public bool IsAllowed(UserStatuses userStatus) => _session.IsAllowed(userStatus);

        /// <summary>
        /// Checks if the shop's user is allowed to create a new order.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        public string makeOrder()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.makeOrder();
            else
                return $"{warningMessage.Keys.First()}{warningMessage.Values.First()}";
        }

        /// <summary>
        /// Gets a dictionary with all user commands descriptors.
        /// </summary>
        /// <returns>A dictionary with all user commands descriptors.</returns>
        public Dictionary<string, string[]> man() => _session.man();

        /// <summary>
        /// Gets the information on a particular user command.
        /// </summary>
        /// <param name="commandName">The name of the command of interest.</param>
        /// <returns>The information on a particular user command.</returns>
        public Dictionary<string, string[]> man(string commandName) => _session.man(commandName);

        /// <summary>
        /// Conducts a preliminary check if a new user registration is possible with the provided data. 
        /// </summary>
        /// <param name="email">New client's email.</param>
        /// <param name="password">New client's password.</param>
        /// <param name="passwordConfirmation">New client's password confirmation.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string maySignUp(string email, string password, string passwordConfirmation) => _session.maySignUp(email, password, passwordConfirmation);

        /// <summary>
        /// Checks if the shop's user is allowed to remove an order item from the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>Updated data on the current cart.</returns>
        public Dictionary<string, string> removeFromCart(string productID)
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.removeFromCart(productID);
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the shop's user is allowed to remove the product from the system.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string removeProduct(string productID)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.removeProduct(productID);
            else
                return $"{warningMessage.Keys.First()}{warningMessage.Values.First()}";
        }

        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="productName">The name of the product of interest.</param>
        /// <returns>All the information on the products that had been found in the system.</returns>
        public Dictionary<string, string> searchByName(string productName) => _session.searchByName(productName);

        /// <summary>
        /// Checks if there is the order with the specified unique identifier in the system. 
        /// </summary>
        /// <param name="orderID">The order's specified unique identifier.</param>
        /// <returns>True if the order had already been added to the system, and false otherwise.</returns>
        public bool InOrders(string orderID) => _session.InOrders(orderID);

        /// <summary>
        /// Checks if the shop's user is allowed to change the status of the specific order.
        /// </summary>
        /// <param name="orderID">Order's unique identifier.</param>
        /// <param name="_orderStatus">The status to be set.</param>
        /// <returns>The updated information on the specific order.</returns>
        public Dictionary<string, string> setOrderStatus(string orderID, string _orderStatus)
        {
            if (_session.IsAllowed(UserStatuses.Administrator) &&
                adminOrderStatusesStrings.Any(status => status.ToLower().Equals(_orderStatus.ToLower())))
            {
                return _session.setOrderStatus(orderID, _orderStatus);
            }
            else if (_session.IsAllowed(UserStatuses.Administrator) &&
                    clientOrderStatusesStrings.Any(status => status.ToLower().Equals(_orderStatus.ToLower())))
            {
                if (_session.InOrders(orderID) && _session.GetOwnerId(orderID).Equals(_session.GetLoggedInUser.UserID))
                    return _session.setOrderStatus(orderID, _orderStatus);
                else
                    return warningMessage;

            }
            else if (_session.IsAllowed(UserStatuses.Client) &&
                    clientOrderStatusesStrings.Any(status => status.ToLower().Equals(_orderStatus.ToLower()) &&
                    _session.InOrders(orderID) &&
                   _session.GetOwnerId(orderID).Equals(_session.GetLoggedInUser.UserID)))
            {
                return _session.setOrderStatus(orderID, _orderStatus);
            }
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the shop's user is allowed to retrieve all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="_orderStatus">The order's status.</param>
        /// <returns>All the information on all orders which have the particular status set.</returns>
        public Dictionary<string, string> showAllOrders(string _orderStatus)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.showAllOrders(_orderStatus);
            else
                return warningMessage;
        }

        /// <summary>
        ///  Checks if the shop's user is allowed to retrieve all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>All the information on all orders which correspond to the particular client.</returns>
        public Dictionary<string, string> showClientOrders(string email)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.showClientOrders(email);
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the shop's user is allowed to get personal information of the particular client in the system.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The client's personal information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showClientInfo(string email)
        {
            if (_session.IsAllowed(UserStatuses.Administrator))
                return _session.showClientInfo(email);
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the shop's user is allowed to get all the data on the current cart object.
        /// </summary>
        /// <returns>All the data on the current cart object's state.</returns>
        public Dictionary<string, string> showMyCart()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.showMyCart();
            else
                return warningMessage;
        }

        /// <summary>
        ///  Checks if the shop's user is allowed to get all the personal information that the system has on the logged-in client.
        /// </summary>
        /// <returns>The information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showMyInfo()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.showMyInfo();
            else
                return warningMessage;
        }

        /// <summary>
        /// Checks if the shop's user is allowed to get all the information on orders had been made by logged-in user.
        /// </summary>
        /// <returns>all the information on orders of the logged-in user.</returns>
        public Dictionary<string, string> showMyOrders()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.showMyOrders();
            else
                return warningMessage;
        }

        /// <summary>
        /// Performs a registration of a user in the current shop's session.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="password">Client's password.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string signIn(string email, string password) => _session.signIn(email, password);

        /// <summary>
        /// Checks if the shop's user is allowed to perform a sign out of the particular user from the current shop's session.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        public string signOut()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.signOut();
            else
                return $"{warningMessage.Keys.First()}{warningMessage.Values.First()}";
        }

        /// <summary>
        /// Passes a dictionary with all data needed for new client's registration.
        /// </summary>
        /// <param name="personalInfo">A dictionary with all data needed for new client's registration.</param>
        /// <returns>A welcome or a warning string.</returns>
        public string signUp(Dictionary<string, string> personalInfo) => _session.signUp(personalInfo);

        /// <summary>
        /// Gets the unique identifier of the client who made the order.
        /// </summary>
        /// <param name="orderID">The unique identifier of the order.</param>
        /// <returns>The unique identifier of the client who made the order.</returns>
        public string GetOwnerId(string orderID) => _session.GetOwnerId(orderID);

        /// <summary>
        /// Checks if the shop's user is allowed to retrieve the description of all products available in the shop.
        /// </summary>
        /// <returns>The description of all products available in the shop.</returns>
        public Dictionary<string, string> showAllProducts()
        {
            if (_session.IsAllowed(UserStatuses.Administrator) || _session.IsAllowed(UserStatuses.Client))
                return _session.showAllProducts();
            else
                return warningMessage;
        }

    }
}
