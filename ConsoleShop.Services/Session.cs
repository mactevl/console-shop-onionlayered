﻿using ConsoleShop.Data;
using ConsoleShop.Data.Interfaces;
using ConsoleShop.Data.Services;
using ConsoleShop.Data.Types;
using ConsoleShop.Domain.Interfaces;
using ConsoleShop.Domain.Types;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using static ConsoleShop.Data.Types.Order;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Services
{
    /// <summary>
    /// Represents an abstraction of client's visit to a store.
    /// </summary>
    public class Session : ISession
    {
        /// <summary>
        /// Private readonly field which holds the reference to the system's data.
        /// </summary>
        private readonly IData _systemRepo;

        /// <summary>
        /// A collection of all valid forms of order statuses that can be passed to the session by a client. 
        /// </summary>
        private readonly string[] orderStatusesStrings = { "New", "PaymentReceived", "Payment received", "Sent", "CanceledByUser", "Canceled by user", "CanceledByAdministrator", "Canceled by administrator", "Received", "Completed" };

        /// <summary>
        /// Private field that holds a reference to logged-in User type object.
        /// </summary>
        private User logedInUser;

        /// <summary>
        /// A public getter for a logged-in user.
        /// </summary>
        public User GetLoggedInUser { get => logedInUser; }

        /// <summary>
        /// A private property with a getter and a setter. Setter is used to assign logged-in user and all corresponding properties.
        /// </summary>
        private User LoggedInUser
        {
            get
            {
                return logedInUser;
            }

            set
            {
                logedInUser = value;
                UserStatus = logedInUser.UserStatus;
                UserID = logedInUser.UserID;
                Email = logedInUser.Email;
            }
        }

        /// <summary>
        /// Session Status according to the status of the particular user being logged-in.
        /// </summary>
        public UserStatuses UserStatus { get; protected set; }

        /// <summary>
        /// The user's unique identifier. A property with a public getter and a private setter.
        /// </summary>
        public string UserID { get; protected set; }

        /// <summary>
        /// The cart of the current session. A property with the public getter and a private setter.
        /// </summary>
        public Cart MyCart { get; protected set; }

        /// <summary>
        /// The email of the logged-in user.
        /// </summary>
        public string Email { get; protected set; }

        /// <summary>
        /// The Session object's constructor. Initializes a new instance of the <see cref="Session"/> class.
        /// </summary>
        /// <param name="dataCenterContainer">A reference to the system's data object.</param>
        public Session(IData dataCenterContainer)
        {
            _systemRepo = dataCenterContainer;
            UserStatus = UserStatuses.Guest;
        }

        /// <summary>
        /// Checks if the session status equals to the status needed to execute the particular command.
        /// </summary>
        /// <param name="userStatus">Status needed to execute the particular command.</param>
        /// <returns>True if the session status is enough, and false otherwise.</returns>
        public bool IsAllowed(UserStatuses userStatus) => GetLoggedInUser?.UserStatus.Equals(userStatus) ?? false;

        /// <summary>
        /// Gets all the information on orders had been made by logged-in user.
        /// </summary>
        /// <returns>all the information on orders of the logged-in user.</returns>
        public Dictionary<string, string> showMyOrders()
        {
            if (LoggedInUser == null)
                return new Dictionary<string, string>() { { "Result: ", "You have to sign in first." } };
            else
            {
                return showClientOrders(LoggedInUser.Email);
            }
        }

        /// <summary>
        /// Gets a dictionary with all user commands descriptors.
        /// </summary>
        /// <returns>A dictionary with all user commands descriptors.</returns>
        public Dictionary<string, string[]> man() => _systemRepo.Manual;

        /// <summary>
        /// Gets the information on a particular user command.
        /// </summary>
        /// <param name="commandName">The name of the command of interest.</param>
        /// <returns>The information on a particular user command.</returns>
        public Dictionary<string, string[]> man(string commandName)
        {
            if (_systemRepo.Manual.ContainsKey(commandName))
                return new Dictionary<string, string[]>() { { commandName, _systemRepo.Manual[commandName] } };
            else
                return new Dictionary<string, string[]>() {{"Result: ",
                    new string[]{ "No such a command has been found!", "" } } };
        }

        /// <summary>
        /// Performs a sign out of the particular user from the current shop's session.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        public string signOut()
        {
            if (logedInUser == null)
                return "You have to be signed in first!";
            else
            {
                string result = $"Looking foreward to see you again, {logedInUser.FirstName} {logedInUser.LastName}!";
                logedInUser = null;
                MyCart = null;
                return result;
            }
        }

        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="productName">The name of the product of interest.</param>
        /// <returns>All the information on the products that had been found in the system.</returns>
        public Dictionary<string, string> searchByName(string productName) => _systemRepo.searchByName(productName);

        /// <summary>
        /// Performs a registration of a user in the current shop's session.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="password">Client's password.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string signIn(string email, string password)
        {

            if (_systemRepo.IsClient(email))
            {
                User tryingSignIn = _systemRepo.GetClient(email);
                if (LoggedInUser == null && tryingSignIn.CheckPassword(password))
                {
                    LoggedInUser = tryingSignIn;
                    return $"Welcome back, {logedInUser.FirstName} {logedInUser.LastName}!";
                }
                else if (LoggedInUser != null)
                {
                    return $"{logedInUser.FirstName} {logedInUser.LastName}, you have to sign out first!";
                }
                else
                {
                    return $"Wrong password!";
                }

            }
            else
                return "Sorry, we dont have any client associated with that email!";
        }

        /// <summary>
        /// Conducts a preliminary check if a new user registration is possible with the provided data. 
        /// </summary>
        /// <param name="email">New client's email.</param>
        /// <param name="password">New client's password.</param>
        /// <param name="passwordConfirmation">New client's password confirmation.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string maySignUp(string email, string password, string passwordConfirmation)
        {
            if (logedInUser != null)
                return "You have to sign out first!";
            else
            {
                if (password != passwordConfirmation)
                    return "The provided passwords do not match! Please, try again.";

                if (_systemRepo.IsClient(email))
                    return "That email had already been registered!";
                else
                {
                    return "true";
                }
            }
        }

        /// <summary>
        /// Passes a dictionary with all data needed for new client's registration.
        /// </summary>
        /// <param name="personalInfo">A dictionary with all data needed for new client's registration.</param>
        /// <returns>A welcome or a warning string.</returns>
        public string signUp(Dictionary<string, string> personalInfo)
        {
            string userID = IDsHasher.GetID(_systemRepo.ShopsUsersCount, false);
            UserStatuses userStatus;
            if (personalInfo["User Status: "].Contains("Client"))
                userStatus = UserStatuses.Client;
            else
                userStatus = UserStatuses.Administrator;
            User newUser = new User(userID, personalInfo["Password: "], userStatus);
            personalInfo.Remove("User Status: ");
            Type userType = typeof(User);
            PropertyInfo[] propertyInfos = userType.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                foreach (var item in personalInfo)
                {
                    if (propertyInfo.Name.ToLower().Contains(item.Key.ToLower().Substring(0, item.Key.IndexOf(':'))) && !propertyInfo.Name.ToLower().Contains("Password") && !propertyInfo.Name.ToLower().Contains("BirthDate"))
                    {
                        propertyInfo.SetValue(newUser, item.Value);
                    }
                    else if (propertyInfo.Name.ToLower().Contains("BirthDate".ToLower()) && item.Key.ToLower().Contains("BirthDate".ToLower()))
                    {
                        propertyInfo.SetValue(newUser, DateTime.Parse(item.Value).Date);
                    }
                }
            }

            _systemRepo.AddClient(newUser.Email, newUser);
            string result = signIn(newUser.Email, personalInfo["Password: "]);
            if (result.Contains("Welcome"))
                return $"{logedInUser.FirstName} {logedInUser.LastName}, we are glad to welcome You in WELDERPOINT!";
            else
                return result;
        }

        /// <summary>
        /// Adds a new order item to the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string addToCart(string productID, int amount)
        {
            MyCart ??= new Cart(UserID, _systemRepo);

            if (MyCart.AddToCart(productID, amount))
            {
                return $"Product \"{_systemRepo.searchByProductId(productID).Name}\" in amount of {amount} items has been added to your Cart.";
            }
            else
                return $"Can not add the product \"{productID}\" to your cart! No such a product in the System.";
        }

        /// <summary>
        /// Gets all the data on the current cart object.
        /// </summary>
        /// <returns>All the data on the current cart object's state.</returns>
        public Dictionary<string, string> showMyCart() => MyCart?.GetInfo() ?? new Dictionary<string, string>()
        {
            { "Result: ", "There are no items yet in your cart!"}
        };

        /// <summary>
        /// Removes all order items from the cart.
        /// </summary>
        /// <returns>A confirmation or warning string.</returns>
        public string clearMyCart() => MyCart?.ClearCart() ?? "There is nothing in your cart yet!";

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        public string makeOrder()
        {
            int orderNumber = _systemRepo.ShopsOrdersCount;

            List<OrderItem> newOrderItems = new List<OrderItem>();
            if (MyCart == null || MyCart.OrderItems.Count == 0) return "You have to put something in your cart first!";
            foreach (OrderItem item in MyCart.OrderItems)
                newOrderItems.Add((OrderItem)item.Clone());

            Order newOrder = new Order(orderId: IDsHasher.GetID(orderNumber + 1),
                            clientId: UserID,
                            orderDate: DateTime.Now,
                            orderTotalSum: MyCart.OrderSum,
                            orderStatus: OrderStatuses.New,
                            orderItems: newOrderItems);

            string orderSum = Math.Round(newOrder.OrderTotalSum / 100.00, 2).ToString("n2");

            if (_systemRepo.AddOrder(logedInUser.Email, newOrder))
            {
                MyCart.ClearCart();
                return $"Thank you! Your order with ID: {newOrder.OrderId} and a total sum of {orderSum} UAH has been created. You'll get a confirmation email with all the details.";
            }

            else
                return "Something went wrong! Can not create a new order! Please, try again later.";
        }

        /// <summary>
        /// Gets all the personal information that the system has on the logged-in client.
        /// </summary>
        /// <returns>The information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showMyInfo()
        {
            if (LoggedInUser == null)
                return new Dictionary<string, string>() { { "Warning: ", "You have to sign in first!" } };
            else
                return _systemRepo.showMyInfo(Email);
        }

        /// <summary>
        /// Changes the values for the fields specified of the User type instance in the current session.
        /// </summary>
        /// <param name="fieldName">The field name that should be changed.</param>
        /// <param name="value">A new value for the field.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeMyInfo(string fieldName, string value)
        {
            switch (fieldName.ToLower())
            {
                case "firstname":
                    logedInUser.FirstName = value;
                    return logedInUser.GetInfo();

                case "lastname":
                    logedInUser.LastName = value;
                    return logedInUser.GetInfo();

                case "birthdate":
                    logedInUser.BirthDate = DateTime.Parse(value);
                    return logedInUser.GetInfo();

                case "phonenumber":
                    logedInUser.PhoneNumber = value;
                    return logedInUser.GetInfo();

                case "email":
                    if (_systemRepo.HasOrders(LoggedInUser.Email))
                    {
                        _systemRepo.MoveOrders(LoggedInUser.Email, value);
                    }
                    _systemRepo.changeMyInfo(LoggedInUser.Email, "Email", value);
                    LoggedInUser = _systemRepo.GetClient(value);
                    return logedInUser.GetInfo();

                case "password":
                    logedInUser.SetPassword(value);
                    return logedInUser.GetInfo();
                default:
                    return new Dictionary<string, string>() { { "Result: ", "Wrong input! Try again." } };
            }
        }

        /// <summary>
        ///  Retrieves all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>All the information on all orders which correspond to the particular client.</returns>
        public Dictionary<string, string> showClientOrders(string email)
        {
            if (_systemRepo.IsClient(email))
            {
                if (_systemRepo.HasOrders(email))
                    return _systemRepo.showClientOrders(email);
                else
                    return new Dictionary<string, string>() { { "Result: ", "There are no orders yet!" } };
            }
            else
                return new Dictionary<string, string>() { { "Result: ", "There is no client associated with that email in the System!" } };
        }

        /// <summary>
        /// Gets personal information of the particular client in the system.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The client's personal information in a form of a dictionary of strings.</returns>
        public Dictionary<string, string> showClientInfo(string email)
        {
            if (_systemRepo.IsClient(email))
            {
                return _systemRepo.showClientInfo(email);
            }
            else
                return new Dictionary<string, string>() { { "Result: ", "There is no client associated with that email in the System!" } };

        }

        /// <summary>
        /// Retrieves all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="_orderStatus">The order's status.</param>
        /// <returns>All the information on all orders which have the particular status set.</returns>
        public Dictionary<string, string> showAllOrders(string _orderStatus)
        {
            if (orderStatusesStrings.Any(status => status.ToLower().Equals(_orderStatus.ToLower())))
            {
                if (_orderStatus.ToLower().Contains("PaymentReceived".ToLower()) ||
                    _orderStatus.ToLower().Contains("Payment received".ToLower()))
                    _orderStatus = "PaymentReceived";
                if (_orderStatus.ToLower().Contains("CanceledByUser".ToLower()) ||
                    _orderStatus.ToLower().Contains("Canceled by user".ToLower()))
                    _orderStatus = "CanceledByUser";
                if (_orderStatus.ToLower().Contains("CanceledByAdministrator".ToLower()) ||
                    _orderStatus.ToLower().Contains("Canceled by administrator".ToLower()))
                    _orderStatus = "CanceledByAdministrator";
                OrderStatuses orderStatus = (OrderStatuses)Enum.Parse(typeof(OrderStatuses), _orderStatus);
                return _systemRepo.showAllOrders(orderStatus);
            }
            else
                return new Dictionary<string, string>() { { "Warning!", "System does not recognize that status." } };
        }

        /// <summary>
        /// Retrieves the description of all products available in the shop.
        /// </summary>
        /// <returns>The description of all products available in the shop.</returns>
        public Dictionary<string, string> showAllProducts() => _systemRepo.showAllProducts();

        /// <summary>
        /// Changes the status of the specific order.
        /// </summary>
        /// <param name="orderID">Order's unique identifier.</param>
        /// <param name="_orderStatus">The status to be set.</param>
        /// <returns>The updated information on the specific order.</returns>
        public Dictionary<string, string> setOrderStatus(string orderID, string _orderStatus)
        {
            if (orderStatusesStrings.Any(status => status.ToLower().Contains(_orderStatus.ToLower())))
            {
                if (_orderStatus.ToLower().Contains("PaymentReceived".ToLower()) ||
                    _orderStatus.ToLower().Contains("Payment received".ToLower()))
                    _orderStatus = "PaymentReceived";
                if (_orderStatus.ToLower().Contains("CanceledByUser".ToLower()) ||
                    _orderStatus.ToLower().Contains("Canceled by user".ToLower()))
                    _orderStatus = "CanceledByUser";
                if (_orderStatus.ToLower().Contains("CanceledByAdministrator".ToLower()) ||
                    _orderStatus.ToLower().Contains("Canceled by administrator".ToLower()))
                    _orderStatus = "CanceledByAdministrator";
                OrderStatuses orderStatus = (OrderStatuses)Enum.Parse(typeof(OrderStatuses), _orderStatus);
                return _systemRepo.setOrderStatus(orderID, orderStatus);
            }
            else
                return new Dictionary<string, string>() { { "Warning!", "System does not recognize that status." } };

        }

        /// <summary>
        /// Edits the amount of the particular product in the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>Updated data on the current cart.</returns>
        public Dictionary<string, string> editItemInCart(string productID, int amount) => MyCart.editItemInCart(productID, amount);

        /// <summary>
        /// Removes an order item from the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>Updated data on the current cart.</returns>
        public Dictionary<string, string> removeFromCart(string productID) => MyCart.RemoveFromCart(productID);

        /// <summary>
        /// Adds a new product to the system.
        /// </summary>
        /// <param name="productState">A dictionary with all the data needed for a new product registration.</param>
        /// <returns>All the information on the new product.</returns>
        public Dictionary<string, string> addNewProduct(Dictionary<string, string> productState)
        {
            int systemProductsCount = _systemRepo.ShopsProductsCount;
            Product newProduct = new Product(systemProductsCount + 1);
            foreach (var item in productState)
            {
                if (!item.Key.Contains("Price") && !item.Key.Contains("Discount"))
                    newProduct.EditInfo(item.Key, item.Value);
                else
                    if (Regex.IsMatch(item.Value, @"\d+"))
                        newProduct.EditInfo(item.Key, int.Parse(item.Value));
                else
                    return new Dictionary<string, string>() { { "Result: ", "Wrong Input! Can not add new product to the System!" } };
            }
            if (_systemRepo.AddProduct(newProduct.ID, newProduct))
                return newProduct.GetInfo();
            else
                return new Dictionary<string, string>() { { "Result: ", "Something went wrong! Can not add new product to the System!" } };
        }

        /// <summary>
        /// Removes the product from the system.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>A confirmation or a warning string.</returns>
        public string removeProduct(string productID)
        {
            if (_systemRepo.removeProduct(productID))
                return $"Result: Product with ID: {productID} has been deleted from the system!";
            else
                return "Result: There is no such a product in the system!";
        }

        /// <summary>
        /// Changes the information of the particular product through updating its fields.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific product.</returns>
        public Dictionary<string, string> changeProductInfo(string productID, string fieldName, string value)
        {
            Product editProduct;
            if (_systemRepo.IsInProducts(productID))
                editProduct = _systemRepo.searchByProductId(productID);
            else
                return new Dictionary<string, string>() { { "Result: ", "There is no such a product in the System!" } };

            switch (fieldName.ToLower())
            {
                case "name":
                    editProduct.Name = value;
                    return _systemRepo.searchByProductId(productID).GetInfo();

                case "category":
                    editProduct.Category = value;
                    return _systemRepo.searchByProductId(productID).GetInfo();

                case "description":
                    editProduct.Description = value;
                    return _systemRepo.searchByProductId(productID).GetInfo();

                case "price":
                    editProduct.Price = int.Parse(value);
                    return _systemRepo.searchByProductId(productID).GetInfo();

                case "discount":
                    editProduct.Discount = int.Parse(value);
                    return _systemRepo.searchByProductId(productID).GetInfo();

                default:
                    return new Dictionary<string, string>() { { "Result: ", "Wrong input! Try again." } };
            }
        }

        /// <summary>
        /// Changes the state of the User type instance through updating its fields with new values provided.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        public Dictionary<string, string> changeClientsInfo(string email, string fieldName, string value)
        {
            User editUser;
            if (_systemRepo.IsClient(email))
                editUser = _systemRepo.GetClient(email);
            else
                return new Dictionary<string, string>() { { "Result: ", "There is no such a user in the System!" } };

            switch (fieldName.ToLower())
            {
                case "firstname":
                    editUser.FirstName = value;
                    return editUser.GetInfo();

                case "lastname":
                    editUser.LastName = value;
                    return editUser.GetInfo();

                case "birthdate":
                    editUser.BirthDate = DateTime.Parse(value);
                    return editUser.GetInfo();

                case "phonenumber":
                    editUser.PhoneNumber = value;
                    return editUser.GetInfo();

                case "email":
                    editUser.Email = value;
                    return editUser.GetInfo();

                case "password":
                    editUser.SetPassword(value);
                    return editUser.GetInfo();

                default:
                    return new Dictionary<string, string>() { { "Result: ", "Wrong input! Try again." } };
            }
        }

        /// <summary>
        /// Checks if a new administrator registration attempt is valid.
        /// </summary>
        /// <param name="masterPassword">Shop's master key, provided by the shop's user.</param>
        /// <returns>True if the master key is correct, and false otherwise.</returns>
        public bool checkCredentials(string masterPassword) => _systemRepo.MasterKey(masterPassword);

        /// <summary>
        /// Checks if there is the order with the specified unique identifier in the system. 
        /// </summary>
        /// <param name="orderID">The order's specified unique identifier.</param>
        /// <returns>True if the order had already been added to the system, and false otherwise.</returns>
        public bool InOrders(string orderID) => _systemRepo.InOrders(orderID);

        /// <summary>
        /// Gets the unique identifier of the client who made the order.
        /// </summary>
        /// <param name="orderID">The unique identifier of the order.</param>
        /// <returns>The unique identifier of the client who made the order.</returns>
        public string GetOwnerId(string orderID) => _systemRepo.GetOwnerId(orderID);

    }
}
