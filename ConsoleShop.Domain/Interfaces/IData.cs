﻿using ConsoleShop.Data.Services;
using ConsoleShop.Data.Types;
using System;
using System.Collections.Generic;
using System.Text;
using static ConsoleShop.Data.Types.Order;

namespace ConsoleShop.Data.Interfaces
{
    /// <summary>
    /// THe interface for calling Data Layer methods.
    /// </summary>
    public interface IData
    {
        //IData
        /// <summary>
        /// A generic method which counts entries in main collections of the type(users, products, orders).
        /// </summary>
        /// <typeparam name="T">Generic type for values of main dictionaries.</typeparam>
        /// <param name="entriesCollection">A reference to the dictionary, where key-value pairs should be counted.</param>
        /// <returns>The number of entries in the specified dictionary.</returns>
        int GetShopDataEntriesCount<T>(Dictionary<string, T> entriesCollection);

        /// <summary>
        /// Conducts a search of the specified product by its unique identifier in the products' collection.
        /// </summary>
        /// <param name="productId">The product's unique identifier.</param>
        /// <returns>The corresponding instance of the Product type.</returns>
        Product searchByProductId(string productId);

        /// <summary>
        /// Checks if there is a client had already been registered in the system with specified email. 
        /// </summary>
        /// <param name="email">An email of the guest or a shop's client.</param>
        /// <returns>True if there is a client in the system with the email specified.</returns>
        bool IsClient(string email);

        /// <summary>
        /// Gets the instance of the type User corresponding to the email specified.
        /// </summary>
        /// <param name="email">An email of a shop's client.</param>
        /// <returns>The instance of the type User corresponding to the email specified.</returns>
        User GetClient(string email);

        /// <summary>
        /// Adds a new client to the system.
        /// </summary>
        /// <param name="email">An email of a guest.</param>
        /// <param name="user">The new instance of the User class that should be added to the system.</param>
        /// <returns>Confirmation or warning string</returns>
        string AddClient(string email, User user);

        /// <summary>
        /// Adds a new order to the system.
        /// </summary>
        /// <param name="email">An email of the client.</param>
        /// <param name="order">The new instance of the Order class that should be added to the system.</param>
        /// <returns>True if the order had been added to the system, and false otherwise.</returns>
        bool AddOrder(string email, Order order);

        /// <summary>
        /// Checks if there is the order with the specified unique identifier in the system. 
        /// </summary>
        /// <param name="orderID">The order's specified unique identifier.</param>
        /// <returns>True if the order had already been added to the system, and false otherwise.</returns>
        bool InOrders(string orderID);

        /// <summary>
        /// Checks if the client has any orders.
        /// </summary>
        /// <param name="email">An email of the client.</param>
        /// <returns>True if the client has any orders, and false otherwise.</returns>
        bool HasOrders(string email);

        /// <summary>
        /// Moves orders in the ShopsOrders dictionary under the updated email.
        /// </summary>
        /// <param name="oldShopsOrdersEmailKey">Old user's email</param>
        /// <param name="newShopsOrdersEmailKey">New user's email</param>
        void MoveOrders(string oldShopsOrdersEmailKey, string newShopsOrdersEmailKey);

        /// <summary>
        /// Checks if the system possesses the specified product.
        /// </summary>
        /// <param name="productId">The unique product's identifier.</param>
        /// <returns>True if thehe is the product in the system, and false otherwise.</returns>
        bool IsInProducts(string productId);

        /// <summary>
        /// Gets the unique identifier of the client who made the order.
        /// </summary>
        /// <param name="orderID">The unique identifier of the order.</param>
        /// <returns>The unique identifier of the client who made the order.</returns>
        string GetOwnerId(string orderID);

        /// <summary>
        /// A property with a getter which returns the system's manual collection.
        /// </summary>
        Dictionary<string, string[]> Manual { get; }

        //IGuestData
        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="productName">The name of the product of interest.</param>
        /// <returns>All the information on the products that had been found in the system.</returns>
        Dictionary<string, string> searchByName(string productName);

        //IClientData
        /// <summary>
        /// A property with a getter. Retrieves the number of entries in orders dictionary.
        /// </summary>
        int ShopsOrdersCount { get; }

        /// <summary>
        /// A property with a getter. Retrieves the number of entries in products dictionary.
        /// </summary>
        int ShopsProductsCount { get; }

        /// <summary>
        /// A property with a getter. Retrieves the number of entries in users dictionary.
        /// </summary>
        int ShopsUsersCount { get; }

        /// <summary>
        /// Adds new product to the system.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="newProduct">The new instance of a Product type that should be added to the system.</param>
        /// <returns>True if the product had been added to the system, and false otherwise.</returns>
        bool AddProduct(string productID, Product newProduct);

        /// <summary>
        /// Adds a new product to the system.
        /// </summary>
        /// <param name="name">The new product's name.</param>
        /// <param name="category">The new product's category.</param>
        /// <param name="description">The new product's description.</param>
        /// <param name="price">The new product's price.</param>
        /// <param name="discount">The new product's discount.</param>
        /// <returns>All the information on the new product.</returns>
        Dictionary<string, string> AddProduct(string name, string category, string description, int price, int discount);

        /// <summary>
        /// Gets the all information on all products presented in the shop. 
        /// </summary>
        /// <returns>The information on all products in the shop in the form of a dictionary.</returns>
        Dictionary<string, string> showAllProducts();

        /// <summary>
        /// Gets the description of all orders that had been made by the specific client.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The information in the form of a dictionary on all orders that had been made by the client.</returns>
        Dictionary<string, string> showMyOrders(string email);

        /// <summary>
        /// Changes the status of the specific order.
        /// </summary>
        /// <param name="orderID">Order's unique identifier.</param>
        /// <param name="status">The status to be set.</param>
        /// <returns>The updated information on the specific order.</returns>
        Dictionary<string, string> setOrderStatus(string orderID, OrderStatuses status);

        /// <summary>
        /// Gets all the personal information that the system has on the specific client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>The information in a form of a dictionary of strings.</returns>
        Dictionary<string, string> showMyInfo(string email);

        /// <summary>
        /// Changes the values for the fields specified of the User type instance.
        /// </summary>
        /// <typeparam name="T">A generic type for values of User type instance fields.</typeparam>
        /// <param name="email">The client's email.</param>
        /// <param name="fieldName">The field name that should be changed.</param>
        /// <param name="value">A new value for the field.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        Dictionary<string, string> changeMyInfo<T>(string email, string fieldName, T value);

        //IAdministratorData
        /// <summary>
        /// Gets personal information of the particular client in the system.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The client's personal information in a form of a dictionary of strings.</returns>
        Dictionary<string, string> showClientInfo(string email);

        /// <summary>
        /// Changes the state of the User type instance through updating its fields with new values provided.
        /// </summary>
        /// <typeparam name="T">A generic type for values of User type instance fields.</typeparam>
        /// <param name="email">Client's email.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        Dictionary<string, string> changeClientInfo<T>(string email, string fieldName, T value);

        /// <summary>
        /// Changes the information of the particular product through updating its fields.
        /// </summary>
        /// <typeparam name="T">A generic type for values of Product type instance fields.</typeparam>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific product.</returns>
        Dictionary<string, string> changeProductInfo<T>(string productID, string fieldName, T value);

        /// <summary>
        /// Removes the specified product from the system.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <returns>True if the product had been deleted successfully, and false otherwise. </returns>
        bool removeProduct(string productID);

        /// <summary>
        /// Retrieves all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="status">the order's status.</param>
        /// <returns>All the information on all orders which have the particular status set.</returns>
        Dictionary<string, string> showAllOrders(OrderStatuses status);

        /// <summary>
        ///  Retrieves all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>All the information on all orders which correspond to the particular client.</returns>
        Dictionary<string, string> showClientOrders(string email);

        /// <summary>
        /// Checks the master key provided within the process of registration of a new administrator. 
        /// </summary>
        /// <param name="masterPassword">The shop's master key.</param>
        /// <returns>True if the provided password identical to the system's one, and false otherwise.</returns>
        bool MasterKey(string masterPassword);

    }
}
