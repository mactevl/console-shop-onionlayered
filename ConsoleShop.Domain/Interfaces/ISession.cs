﻿using ConsoleShop.Data.Types;
using System;
using System.Collections.Generic;
using System.Text;
using static ConsoleShop.Data.Types.Order;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Domain.Interfaces
{
    /// <summary>
    /// An interface for BLL. For both: Session and SessionProxy classes.
    /// </summary>
     public interface ISession
    {
        //IGuestSession
        /// <summary>
        /// Searches the products in the system by the name specified.
        /// </summary>
        /// <param name="productName">The name of the product of interest.</param>
        /// <returns>All the information on the products that had been found in the system.</returns>
        Dictionary<string, string> searchByName(string productName);

        /// <summary>
        /// Performs a registration of a user in the current shop's session.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="password">Client's password.</param>
        /// <returns>A confirmation or a warning string.</returns>
         string signIn(string email, string password);

        /// <summary>
        /// Conducts a preliminary check if a new user registration is possible with the provided data. 
        /// </summary>
        /// <param name="email">New client's email.</param>
        /// <param name="password">New client's password.</param>
        /// <param name="passwordConfirmation">New client's password confirmation.</param>
        /// <returns>A confirmation or a warning string.</returns>
        string maySignUp(string email, string password, string passwordConfirmation);

        /// <summary>
        /// Passes a dictionary with all data needed for new client's registration.
        /// </summary>
        /// <param name="personalInfo">A dictionary with all data needed for new client's registration.</param>
        /// <returns>A welcome or a warning string.</returns>
        string signUp(Dictionary<string, string> personalInfo);

        /// <summary>
        /// Gets a dictionary with all user commands descriptors.
        /// </summary>
        /// <returns>A dictionary with all user commands descriptors.</returns>
        Dictionary<string, string[]> man();

        /// <summary>
        /// Gets the information on a particular user command.
        /// </summary>
        /// <param name="commandName">The name of the command of interest.</param>
        /// <returns>The information on a particular user command.</returns>
        Dictionary<string, string[]> man(string commandName);

        /// <summary>
        /// Performs a check if the particular command may be executed in accordance with shop's user credentials.
        /// </summary>
        /// <param name="userStatus">User's status(a Guest, a Client or an Administrator).</param>
        /// <returns>True if command execution is allowed, and false otherwise.</returns>
         bool IsAllowed(UserStatuses userStatus);

        /// <summary>
        /// Gets the unique identifier of the client who made the order.
        /// </summary>
        /// <param name="orderID">The unique identifier of the order.</param>
        /// <returns>The unique identifier of the client who made the order.</returns>
        string GetOwnerId(string orderID);

        /// <summary>
        /// A public property with a getter to retrieve the current logged-in user.
        /// </summary>
        public User GetLoggedInUser { get; }

        /// <summary>
        /// Checks if there is the order with the specified unique identifier in the system. 
        /// </summary>
        /// <param name="orderID">The order's specified unique identifier.</param>
        /// <returns>True if the order had already been added to the system, and false otherwise.</returns>
        public bool InOrders(string orderID);

        //IClientSession
        /// <summary>
        /// Performs a sign out of the particular user from the current shop's session.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        string signOut();

        /// <summary>
        /// Adds a new order item to the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>A confirmation or a warning string.</returns>
         string addToCart(string productID, int amount);

        /// <summary>
        /// Edits the amount of the particular product in the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount.</param>
        /// <returns>Updated data on the current cart.</returns>
        Dictionary<string, string> editItemInCart(string productID, int amount);

        /// <summary>
        /// Removes an order item from the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>Updated data on the current cart.</returns>
        Dictionary<string, string> removeFromCart(string productID);

        /// <summary>
        /// Gets all the data on the current cart object.
        /// </summary>
        /// <returns>All the data on the current cart object's state.</returns>
        Dictionary<string, string> showMyCart();

        /// <summary>
        /// Gets all the information on orders had been made by logged-in user.
        /// </summary>
        /// <returns>all the information on orders of the logged-in user.</returns>
        Dictionary<string, string> showMyOrders();

        /// <summary>
        /// Retrieves the description of all products available in the shop.
        /// </summary>
        /// <returns>The description of all products available in the shop.</returns>
        Dictionary<string, string> showAllProducts();

        /// <summary>
        /// Removes all order items from the cart.
        /// </summary>
        /// <returns>A confirmation or warning string.</returns>
         string clearMyCart();

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
         string makeOrder();

        /// <summary>
        /// Gets all the personal information that the system has on the logged-in client.
        /// </summary>
        /// <returns>The information in a form of a dictionary of strings.</returns>
        Dictionary<string, string> showMyInfo();

        /// <summary>
        /// Changes the values for the fields specified of the User type instance in the current session.
        /// </summary>
        /// <param name="fieldName">The field name that should be changed.</param>
        /// <param name="value">A new value for the field.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        Dictionary<string, string> changeMyInfo(string fieldName, string value);


        //IAdminSession
        /// <summary>
        /// Adds a new product to the system.
        /// </summary>
        /// <param name="productState">A dictionary with all the data needed for a new product registration.</param>
        /// <returns>All the information on the new product.</returns>
        Dictionary<string, string> addNewProduct(Dictionary<string, string> productState);

        /// <summary>
        /// Removes the product from the system.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>A confirmation or a warning string.</returns>
         string removeProduct(string productID);

        /// <summary>
        ///  Retrieves all the information on all orders which had been made by a particular client.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <returns>All the information on all orders which correspond to the particular client.</returns>
        Dictionary<string, string> showClientOrders(string email);

        /// <summary>
        /// Gets personal information of the particular client in the system.
        /// </summary>
        /// <param name="email">The client's email.</param>
        /// <returns>The client's personal information in a form of a dictionary of strings.</returns>
        Dictionary<string, string> showClientInfo(string email);

        /// <summary>
        /// Retrieves all the information on all orders which have the particular status set. 
        /// </summary>
        /// <param name="_orderStatus">The order's status.</param>
        /// <returns>All the information on all orders which have the particular status set.</returns>
        Dictionary<string, string> showAllOrders(string _orderStatus);

        /// <summary>
        /// Changes the status of the specific order.
        /// </summary>
        /// <param name="orderID">Order's unique identifier.</param>
        /// <param name="_orderStatus">The status to be set.</param>
        /// <returns>The updated information on the specific order.</returns>
        Dictionary<string, string> setOrderStatus(string orderID, string _orderStatus);

        /// <summary>
        /// Changes the state of the User type instance through updating its fields with new values provided.
        /// </summary>
        /// <param name="email">Client's email.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific client.</returns>
        Dictionary<string, string> changeClientsInfo(string email, string fieldName, string value);

        /// <summary>
        /// Changes the information of the particular product through updating its fields.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="fieldName">The field name that should be updated.</param>
        /// <param name="value">A new value for the field of the instance.</param>
        /// <returns>The updated personal information of the specific product.</returns>
        Dictionary<string, string> changeProductInfo(string productID, string fieldName, string value);

        /// <summary>
        /// Checks if a new administrator registration attempt is valid.
        /// </summary>
        /// <param name="masterPassword">Shop's master key, provided by the shop's user.</param>
        /// <returns>True if the master key is correct, and false otherwise.</returns>
        bool checkCredentials(string masterPassword);
    }
}
