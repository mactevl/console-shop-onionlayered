﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// Represents an abstraction of the Order object of the domain field.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// The unique client identifier. A getter and a private setter.
        /// </summary>
        public string ClientId { get; private set; }
        /// <summary>
        /// The unique order identifier. A getter and a private setter.
        /// </summary>
        public string OrderId { get; private set; }
        /// <summary>
        /// The exact date when the order had been made. A getter and a private setter.
        /// </summary>
        public DateTime OrderDate { get; private set; }
        /// <summary>
        /// Order's total sum. A getter and a private setter.
        /// </summary>
        public int OrderTotalSum { get; private set; }
        /// <summary>
        /// The status of the order. A getter and a private setter.
        /// </summary>
        public OrderStatuses OrderStatus { get; private set; }
        /// <summary>
        /// Contains all items included in the order. A getter and a private setter.
        /// </summary>
        public List<OrderItem> OrderItems { get; private set; }

        /// <summary>
        /// A set of constants representing order statuses.
        /// </summary>
        public enum OrderStatuses 
        {   /// <summary>
            /// An order status for all newly created orders.
            /// </summary>
            New,

            /// <summary>
            /// An order status for all orders which had been paid for.
            /// </summary>
            PaymentReceived,

            /// <summary>
            /// An order status for all sent orders.
            /// </summary>
            Sent,

            /// <summary>
            /// An order status for all orders which had been canceled by the user.
            /// </summary>
            CanceledByUser,

            /// <summary>
            /// An order status for all orders which had been canceled by an administrator.
            /// </summary>
            CanceledByAdministrator,

            /// <summary>
            /// An order status for all orders which had been received by the user.
            /// </summary>
            Received,

            /// <summary>
            /// An order status for all orders which had been successfully completed.
            /// </summary>
            Completed
        }

        /// <summary>
        /// A dictionary that maps order statuses names to its corresponding strings for correct representation of statuses in the UI. Used By GetInfo method.
        /// </summary>
        private readonly Dictionary<string, string> orderStatusesMapToString = new Dictionary<string, string>()
        {
           {"PaymentReceived", "Payment received"},
           {"CanceledByUser", "Canceled by user"},
           {"CanceledByAdministrator", "Canceled by administrator"},
           {"New", "New"},
           {"Sent", "Sent"},
           {"Received", "Received"},
           {"Completed", "Completed"},
        };

        /// <summary>
        /// The Order class constructor. Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        /// <param name="clientId">The client's unique identifier.</param>
        /// <param name="orderId">The order's unique identifier.</param>
        /// <param name="orderDate">The exact date when the order had been made.</param>
        /// <param name="orderTotalSum">Order's total sum.</param>
        /// <param name="orderStatus">The status of the order.</param>
        /// <param name="orderItems">Contains all items included in the order.</param>
        public Order(string clientId,
                     string orderId,
                     DateTime orderDate,
                     int orderTotalSum,
                     OrderStatuses orderStatus,
                     List<OrderItem> orderItems)
        {
            this.ClientId = clientId;
            this.OrderId = orderId;
            this.OrderDate = orderDate;
            this.OrderTotalSum = orderTotalSum;
            this.OrderStatus = orderStatus;
            this.OrderItems = orderItems;
        }

        /// <summary>
        /// Gets all the information on the Order instance state.
        /// </summary>
        /// <returns>A dictionary containing the field name as a key and its description as a value.</returns>
        public Dictionary<string, string> GetInfo()
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            Type orderType = typeof(Order);
            PropertyInfo[] propertyInfos = orderType.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                string name;
                string value;
                if (propertyInfo.Name == "OrderTotalSum")
                {
                    name = propertyInfo.Name;
                    double valueOrderTotalSum = Math.Round((int)propertyInfo.GetValue(this) / 100.00, 2);
                    info.Add(name, valueOrderTotalSum.ToString("n2") + " UAH");
                    continue;
                }
                if (propertyInfo.Name == "OrderStatus")
                {
                    name = propertyInfo.Name;
                    value = orderStatusesMapToString[propertyInfo.GetValue(this).ToString()];
                    info.Add(name, value);
                    continue;
                }
                if (propertyInfo.Name == "OrderItems")
                {
                    GetOrderItemsInfo(propertyInfo, info);
                    continue;
                }

                name = propertyInfo.Name;
                value = propertyInfo.GetValue(this).ToString();
                info.Add(name, value);
            }
            return info;

        }

        /// <summary>
        /// Gets all the information on every item included in the order.
        /// </summary>
        /// <param name="propertyInfo">A reference to the corresponding metadata of Order type, which in this case includes a field with the list of all order items.</param>
        /// <param name="orderInfoDictionary">A reference to the dictionary, where all the information on order items should be added.</param>
        public void GetOrderItemsInfo( PropertyInfo propertyInfo, Dictionary<string, string> orderInfoDictionary)
        {
            var orderItemsList = propertyInfo.GetValue(this) as List<OrderItem>;
            foreach (OrderItem orderItem in orderItemsList)
            {
                foreach (var orderItemProp in orderItem.GetInfo())
                    orderInfoDictionary.Add($"Product ID: {orderItem.Product.ID}" + " | " + orderItemProp.Key, orderItemProp.Value);
            }
        }

        /// <summary>
        /// Sets the status of the Order instance.
        /// </summary>
        /// <param name="status">The status which should be used for the order status to be set.</param>
        public void SetStatus(OrderStatuses status) => this.OrderStatus = status;
        
    }
}
