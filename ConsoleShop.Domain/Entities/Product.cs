﻿using ConsoleShop.Data.Services;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// A type that represents product objects of the domain field.
    /// </summary>
    public class Product : ICloneable
    {
        /// <summary>
        /// The unique identifier of the product. A public field with a public getter and a setter.
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// The name of the product. A public field with a getter and a setter.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        ///  The category of the product. A public field with a getter and a setter.
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// The description of the product. A public field with a getter and a setter.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Product's price. A public field with a getter and a setter.
        /// </summary>
        public int Price { get; set; }
        /// <summary>
        /// Product's discount. A public field with a getter and a setter.
        /// </summary>
        public int Discount { get; set; }

        /// <summary>
        /// The Product type constructor. Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        public Product(int productID)
        {
            this.ID = IDsHasher.GetID(productID, false);
        }

        /// <summary>
        /// A parameterless constructor for a Product type.
        /// </summary>
        public Product() {}

        /// <summary>
        /// The Product type constructor. Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productID">The unique identifier of the product.</param>
        /// <param name="name">A name of the product.</param>
        /// <param name="category">A category of the product.</param>
        /// <param name="description">Product's description.</param>
        /// <param name="price">Product's price.</param>
        /// <param name="discount">Product's discount.</param>
        public Product(string productID,
                        string name,
                        string category,
                        string description,
                        int price,
                        int discount = 0)
        {
            this.ID = productID;
            this.Name = name;
            this.Category = category;
            this.Description = description;
            this.Price = price;
            this.Discount = discount;
        }

        /// <summary>
        /// Method that implements the IClonable interface. Performs a shallow copy of the Product instance.
        /// </summary>
        /// <returns>Returns a shallow copy of the Product instance.</returns>
        public object Clone() => MemberwiseClone();
        /// <summary>
        /// Gets all the information on the Product object.
        /// </summary>
        /// <returns>Returns the state of the Product object in a form of dictionary.</returns>
        public Dictionary<string, string> GetInfo()
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            Type productType = typeof(Product);
            PropertyInfo[] propertyInfos = productType.GetProperties();
            foreach(PropertyInfo propertyInfo in propertyInfos)
            {
                string name = propertyInfo.Name;
                if (propertyInfo.Name != "Price")
                {                    
                    string value = propertyInfo.GetValue(this).ToString();
                    info.Add(name, value);
                }
                else
                {
                    double value = Math.Round((int)propertyInfo.GetValue(this) / 100.00, 3);
                    info.Add(name, value.ToString("n2") + " UAH");
                }
            }
            return info;           

        }

        /// <summary>
        /// Gets all the information on the Product object.
        /// </summary>
        /// <returns>Returns the state of the Product object in a form of a string.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\t-Product ID:");
            sb.Append(this.ID);
            sb.Append("\t-Name: ");
            sb.Append(this.Name);
            sb.Append("\t-Price: ");
            sb.Append(Math.Round(this.Price /100.00, 2).ToString("n2") + " UAH");
            sb.Append("\t-Discount: ");
            sb.Append(this.Discount.ToString());
            
            return sb.ToString();
        }
        /// <summary>
        /// A generic method which makes changes to the Product object's state through setting new values for its fields.
        /// </summary>
        /// <typeparam name="T">A generic type for a value of the Product's state field (string or an integer).</typeparam>
        /// <param name="property">A field of the Product type that needs to be changed.</param>
        /// <param name="value">A value of the Product's type field that needs to be changed.</param>
        public void EditInfo<T>(string property, T value)
        {
            Type productType = typeof(Product);
            PropertyInfo propToChange = productType.GetProperty(property);
            propToChange.SetValue(this, value);
        }
    }
}
