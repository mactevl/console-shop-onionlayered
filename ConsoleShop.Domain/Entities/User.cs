﻿using ConsoleShop.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// An abstraction that represents shop's user of the domain field.
    /// </summary>
    public class User : ICloneable
    {
        /// <summary>
        /// A unique identifier of the shop's user. A property with a public getter and a private setter.
        /// </summary>
        public string UserID { get; private set; }
        /// <summary>
        /// A status of the shop's user(a Guest, a Client, or an Administrator). A property with a public getter and a private setter.
        /// </summary>
        public UserStatuses UserStatus { get; private set; }
        /// <summary>
        /// User's first name. A public property with both: a getter and a setter.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// User's last name. A public property with both: a getter and a setter.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// User's date of birth. A public property with both: a getter and a setter.
        /// </summary>
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// User's cellphone number. A public property with both: a getter and a setter.
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// User's email address. A public property with both: a getter and a setter.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User's password in a form of hashed array of bytes. A private property with both: a getter and a setter.
        /// </summary>
        private byte[] Password { get; set; }
        /// <summary>
        /// A set of constants that represents all the foreseen statuses of shop's users.
        /// </summary>
        public enum UserStatuses 
        {   
            /// <summary>
            /// This status is assigned to any visitor without an account.
            /// </summary>
            Guest,

            /// <summary>
            /// This status is assigned to any shop's client.
            /// </summary>
            Client,

            /// <summary>
            /// This status is assigned to any shop's administrator.
            /// </summary>
            Administrator
        };

        /// <summary>
        /// A constructor of the User class. Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="userID">The unique identifier of the user.</param>
        /// <param name="password">A password, provided by the user through a registration process.</param>
        /// <param name="userStatus">A status of the user, identified by the session of the shop.</param>
        public User(string userID,                    
                    string password, 
                    UserStatuses userStatus)
        {
            this.UserID = userID;
            this.UserStatus = userStatus;
            this.FirstName = "FirstName";
            this.LastName = "LastName";
            this.BirthDate = DateTime.Now.Date;
            this.PhoneNumber = "+380000000000";
            this.Email = "default@default.com";
            this.Password = PasswordHasher.GetHash(password);
        }

        /// <summary>
        /// A constructor of the User class. Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="userID">The unique identifier of the user.</param>
        /// <param name="userStatus">A status of the user.</param>
        /// <param name="firstName"> User's first name.</param>
        /// <param name="lastName"> User's last name.</param>
        /// <param name="birthDate">User's date of birth.</param>
        /// <param name="phoneNumber">User's cellphone number(+380 and 9 digits).</param>
        /// <param name="email"> User's email address.</param>
        /// <param name="password">A password, provided by the user through a registration process.</param>
        public User(string userID,
                    UserStatuses userStatus,
                    string firstName,
                    string lastName,
                    DateTime birthDate,
                    string phoneNumber,
                    string email,
                    string password)
        {
            this.UserID = userID;
            this.UserStatus = userStatus;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = birthDate;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
            this.Password = PasswordHasher.GetHash(password);
        }

        /// <summary>
        /// Implements IClonable interface.
        /// </summary>
        /// <returns>A shallow copy of the User instance.</returns>
        public Object Clone() => MemberwiseClone();

        /// <summary>
        /// Gets hashed array of bytes from the password provided by the user while registering.
        /// </summary>
        /// <param name="password">A password, provided by the user through a registration process.</param>
        /// <returns>User's password in a form of hashed array of bytes.</returns>
        public bool SetPassword(string password)
        {
            var currentPassword = this.Password;
            this.Password = PasswordHasher.GetHash(password);
            if (currentPassword != this.Password)
                return true;
            else 
                return false;
        }

        /// <summary>
        /// Gets all the information on the current instance of the User type.
        /// </summary>
        /// <returns>The state of the current instance of the User type in a form of a dictionary.</returns>
        public Dictionary<string, string> GetInfo()
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            Type userType = typeof(User);
            PropertyInfo[] propertyInfos = userType.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                string name = propertyInfo.Name;
                string value = propertyInfo.GetValue(this).ToString();
                info.Add(name, value);
            }
            return info;
        }

        /// <summary>
        /// A generic method which makes changes to the User object's state through setting new values for its fields.
        /// </summary>
        /// <typeparam name="T">A generic type for a value of the User's state field.</typeparam>
        /// <param name="fieldName">A field of the User type that needs to be changed.</param>
        /// <param name="value">A value of the User's type field that needs to be changed.</param>
        public void EditInfo<T>(string fieldName, T value)
        {
            Type userType = typeof(User);
            PropertyInfo[] propertyInfos = userType.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (propertyInfo.Name == fieldName)
                {
                    propertyInfo.SetValue(this, value);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password">A password, provided by the user through a call to the changeMyInfo method.</param>
        /// <returns>A new User's password in a form of hashed array of bytes.</returns>
        public bool CheckPassword(string password)
        {
            if (PasswordHasher.GetHash(password).SequenceEqual(this.Password))
                return true;
            else
                return false;
        }
    }
}
