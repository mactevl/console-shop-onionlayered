﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleShop.Data.Types;
using ConsoleShop.Data.Interfaces;
using ConsoleShop.Data;
using static ConsoleShop.Data.Types.Order;
using System.Reflection;
using System.Linq;

namespace ConsoleShop.Domain.Types
{
    /// <summary>
    /// Represents an abstraction of the Cart object of the domain field.
    /// </summary>
    public class Cart
    {
        /// <summary>
        /// Private readonly field that keeps a value of user unique identifier.
        /// </summary>
        private readonly string _userID;

        /// <summary>
        /// Private readonly field that keeps a reference to the LazyDataSingleton instance.
        /// </summary>
        private readonly IData _systemRepo;

        /// <summary>
        /// A property with a public getter and a private setter for a list of order items. 
        /// </summary>
        public List<OrderItem> OrderItems { get; private set; } = new List<OrderItem>();

        /// <summary>
        /// A property with a public getter and a private setter for an order total sum.
        /// </summary>
        public int OrderSum { get; private set; } = 0;

        /// <summary>
        /// The Cart class constructor. Initializes a new instance of the <see cref="Cart"/> class.
        /// </summary>
        /// <param name="userID">User's unique identifier.</param>
        /// <param name="systemRepo">A reference to the system data object.</param>
        public Cart(string userID, IData systemRepo)
        {
            this._userID = userID;
            this._systemRepo = systemRepo;
        }

        /// <summary>
        /// Updates the product amount in a particular order item of the current instance of the type Cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">The product's amount.</param>
        /// <returns>The updated data of the Cart type instance.</returns>
        public Dictionary<string, string> editItemInCart(string productID, int amount)
        {
            if (this.IsInCart(productID))
            {
                string productName = this.OrderItems.Find(item => item.Product.ID == productID).Product.Name;
                var itemToEdit = this.OrderItems.Find(item => item.Product.ID == productID);
                int currentOrderItemSum = itemToEdit.OrderItemSum;
                itemToEdit.Amount = amount;
                itemToEdit.OrderItemSum = OrderItem.CalculateSum(itemToEdit.Product,amount);
                this.OrderSum -= currentOrderItemSum - itemToEdit.OrderItemSum;
                if (this.OrderItems.Find(item => item.Product.ID == productID).Amount == amount)
                    return this.GetInfo();
                else
                    return new Dictionary<string, string> { { "Result: ", $"Something went wrong! Ca not edit amount for item \"{productName}\" in your order! Try again later." } };
            }
            else
                return new Dictionary<string, string> { { "Result: ", "There is no such a product in your Cart!" } };
        }

        /// <summary>
        /// Checks if the particular product had already been added to the cart.
        /// </summary>
        /// <param name="productId">Product's unique identifier.</param>
        /// <returns>True if there is the product in the cart, and false otherwise.</returns>
        public bool IsInCart(string productId) => this.OrderItems.Any(item => item.Product.ID.Equals(productId));

        /// <summary>
        /// Adds new order item to the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <param name="amount">Product's amount</param>
        /// <returns>True if added successfully, and false otherwise.</returns>
        public bool AddToCart(string productID, int amount)
        {
            int currentItemAmmount = 0;           
            
            if (this.IsInCart(productID))
            {
                var itemExist = OrderItems.First(item => item.Product.ID == productID);
                itemExist.Amount += amount;
                int additionalSum = OrderItem.CalculateSum(itemExist.Product, amount);
                itemExist.OrderItemSum += additionalSum;
                OrderSum += additionalSum;

            }
            else
            {
                if (_systemRepo.IsInProducts(productID))
                {
                    Product productOfInterest = _systemRepo.searchByProductId(productID);
                    OrderItem newItem = new OrderItem(productOfInterest, amount);
                    OrderItems.Add(newItem);
                    OrderSum += newItem.OrderItemSum;
                }
                else
                    return false;
                
            }
            if (OrderItems.First(item => item.Product.ID == productID).Amount != currentItemAmmount)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Removes the order item from the cart.
        /// </summary>
        /// <param name="productID">Product's unique identifier.</param>
        /// <returns>Updated data of the current cart state.</returns>
        public Dictionary<string, string> RemoveFromCart(string productID)
        {
            if (this.IsInCart(productID))
            {
                OrderItem itemToBeRemoved = this.OrderItems.First(item => item.Product.ID == productID);
                string productName = itemToBeRemoved.Product.Name;
                int orderItemSum = itemToBeRemoved.OrderItemSum;

                if (this.OrderItems.Remove(this.OrderItems.Find(item => item.Product.ID == productID)))
                {
                    OrderSum -= orderItemSum;
                    return this.GetInfo();
                }
                    
                else
                    return new Dictionary<string, string>() { { "Result: ", $"Something went wrong! Can not delete the item \"{productName}\" from your cart!" } };
            }
            else
                return new Dictionary<string, string>() { { "Result: ", "There is no such a product in your Cart!" } };
        }

        /// <summary>
        /// Deletes all items from the current cart instance.
        /// </summary>
        /// <returns>A confirmation or a warning string.</returns>
        public string ClearCart()
        {
            OrderItems.Clear();
            OrderSum = 0;
            if (OrderItems.Count == 0)
                return "Your cart has been cleared successfully!";
            else
                return "Something went wrong! Can not clear your cart. Try again later.";
        }

        /// <summary>
        /// Provides all the information on the current cart instance state.
        /// </summary>
        /// <returns>All the information in the form of dictionary on the current cart instance state.</returns>
        public Dictionary<string, string> GetInfo()
        {
            Dictionary<string, string> info = new Dictionary<string, string>();

            info.Add("User ID", this._userID);
            info.Add("Subtotal", Math.Round(this.OrderSum / 100.00, 2).ToString("n2") + " UAH");
            int orderItemsCount = 0;
            foreach (OrderItem item in OrderItems)
            {
                orderItemsCount++;
                info.Add("Item " + $"{orderItemsCount}", "");

                foreach (var pair in item.GetInfo())
                {
                    info.Add($"ProductID: {item.Product.ID} | " + pair.Key, pair.Value);
                }
            }

            return info;

        }
    }
}
