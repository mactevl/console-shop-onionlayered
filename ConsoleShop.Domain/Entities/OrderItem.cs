﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using ConsoleShop.Data.Types;

namespace ConsoleShop.Data.Types
{
    /// <summary>
    /// Represents an abstraction for the items of an Order object of the domain field.
    /// </summary>
    public class OrderItem : ICloneable
    {
        /// <summary>
        /// Gets and setts the object of type Product. A property with public getter and a private setter.
        /// </summary>
        public Product Product { get; private set; }
        /// <summary>
        /// A property that represents an amount of a particular product in the order. A property with public getter and setter.
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// A property with public getter and setter for the order item's total sum.
        /// </summary>
        public int OrderItemSum { get; set; }

        /// <summary>
        /// An Order type constructor, which takes in two arguments: one for the Product and second one is for its amount. Initializes a new instance of the <see cref="OrderItem"/> class.
        /// </summary>
        /// <param name="product">A reference to the instance of the Product type to be included in the order item.</param>
        /// <param name="amount">Product's overall amount in the order.</param>
        public OrderItem(Product product, int amount)
        {
            this.Product = product;
            this.Amount = amount;
            this.OrderItemSum = (product.Price - (product.Price * product.Discount / 100)) * amount;
        }

        /// <summary>
        /// An Order type constructor, which takes in three arguments: one for the Product and second one is for its amount. Initializes a new instance of the <see cref="OrderItem"/> class.
        /// </summary>
        /// <param name="product">A reference to the instance of the Product type to be included in the order item.</param>
        /// <param name="amount">Product's overall amount in the order.</param>
        /// <param name="orderItemSum">Total cost of the product in the order.</param>
        public OrderItem(Product product, int amount, int orderItemSum)
        {
            this.Product = product;
            this.Amount = amount;
            this.OrderItemSum = orderItemSum;
        }

        /// <summary>
        /// Implements the Clone method of Iclonable interface for the purpose of creating a duplicate of the order item.
        /// </summary>
        /// <returns>Returns the duplicate of the instance of the OrderItem type.</returns>
        public object Clone() => new OrderItem((Product)this.Product.Clone(), this.Amount, this.OrderItemSum);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static int CalculateSum(Product product, int amount) => (product.Price - (product.Price * product.Discount / 100)) * amount;

        /// <summary>
        /// Gets all the information on a current instance of the OrderItem typeusing reflection.
        /// </summary>
        /// <returns>A dictionary as set of the key-value descriptors for the OrderItem type. A field as a key and value as a description.</returns>
        public Dictionary<string, string> GetInfo()
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            Type orderItemType = typeof(OrderItem);
            PropertyInfo[] propertyInfos = orderItemType.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (propertyInfo.Name.ToLower().Contains("OrderItemSum".ToLower()))
                {
                    string name = propertyInfo.Name;
                    double value = Math.Round((int)propertyInfo.GetValue(this) / 100.00, 2);
                    info.Add(name, value.ToString("n2") + " UAH");
                }
                else
                {
                    string name = propertyInfo.Name;
                    string value = propertyInfo.GetValue(this).ToString();
                    info.Add(name, value);
                }                
            }
            return info;

        }
    }
}
