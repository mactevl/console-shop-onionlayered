﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleShop.Data.Services
{
    /// <summary>
    /// A static class that represents the service for getting hashed user's passwords. 
    /// </summary>
    public static class PasswordHasher
    {
        /// <summary>
        /// A static method which hashes strings.
        /// </summary>
        /// <param name="password">A password, provided by the user through a registration process.</param>
        /// <returns>User's password in a form of hashed array of bytes.</returns>
        public static byte[] GetHash(string password)
        {
            var passwordBytes = Encoding.Unicode.GetBytes(password);
            var passwordHash = SHA256.Create().ComputeHash(passwordBytes);
            return passwordHash;
        }
    }
}
