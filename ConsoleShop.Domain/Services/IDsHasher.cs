﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using HashidsNet;


namespace ConsoleShop.Data.Services
{
    /// <summary>
    /// A static class that represents the service for getting unique identifiers for shop's users, products, orders etc. 
    /// </summary>
    public static class IDsHasher
    {
        /// <summary>
        /// A public property with a getter and a setter. Only needs for entities to be hard-coded in the system's collections.
        /// </summary>
        public static string Cache { get; set; }

        /// <summary>
        /// A public method which returns the new unique identifier.
        /// </summary>
        /// <param name="toBeEncoded">An integer value to be encoded.</param>
        /// <param name="useCache">A flag which directs, whether the cached value should be returned for every next call to the method.</param>
        /// <returns>The new unique identifier</returns>
        public static string GetID(int toBeEncoded, bool useCache = true)
        {
            if(useCache && Cache != null)
            {
                string cache = Cache;
                Cache = null;
                return cache;
            }
            else
            {
                Random random = new Random();
                string hashSeed = Guid.NewGuid().ToString("D").Substring(0, 13);
                var hashEngine = new Hashids(hashSeed);
                return hashEngine.Encode(toBeEncoded + random.Next(3000000));
            }
        }
    }
}
