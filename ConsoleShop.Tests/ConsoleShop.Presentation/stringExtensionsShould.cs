﻿using ConsoleShop.Presentation.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ConsoleShop.Tests.ConsoleShop.Presentation
{
    public class stringExtensionsShould
    {
        [Theory]
        [InlineData("man")]
        [InlineData("searchByName \"profile pipe\"")]
        [InlineData("signIn mail@gmail.com qQ123!!!")]
        [InlineData("signUp mail@gmail.com qQ123!!! qQ123!!!")]
        [InlineData("showMyCart")]
        [InlineData("addToCart pipe1X 10")]
        [InlineData("makeOrder")]
        [InlineData("showClientInfo mail@gmail.com")]
        [InlineData("changeMyInfo email mail@gmail.com")]
        [InlineData("changeMyInfo firstname John")]
        [InlineData("changeProductInfo pipe1X name \"flat bar\"")]
        [InlineData("changeMyInfo phonenumber +380500789546")]
        [InlineData("changeClientsInfo mail@gmail.com phonenumber +380500789546")]
        [InlineData("showMyOrders")]
        [InlineData("clearMyCart")]
        [InlineData("showAllProducts")]
        [InlineData("addNewProduct")]
        [InlineData("showAllOrders new")]
        [InlineData("showClientOrders mail@gmail.com")]
        [InlineData("removeProduct pipe1X")]
        [InlineData("removeFromCart pipe1X")]
        [InlineData("editItemInCart pipe1X 12")]
        [InlineData("signUpAdmin vital@gmail.com qQ123!!! qQ123!!! WELDERPOINT")]

        public void Validate(string command)
        {
            Assert.True(command.IsValidCommand());
        }

        [Theory]
        [InlineData("")]
        [InlineData(" \" \" ")]
        [InlineData("sign")]
        [InlineData("signIn")]
        [InlineData("signOut 10")]
        [InlineData("searchByName pipe 10")]
        [InlineData("signIn mailgmail.com qQ123!!!")]
        [InlineData("signUp mail@gmail.com qQ123!!! qQ123!!! new")]
        [InlineData("showMyCart 10")]
        [InlineData("addToCart pipe1X new")]
        [InlineData("showClientInfo mailgmail.com")]
        [InlineData("changeMyInfo email 1234")]
        [InlineData("changeMyInfo name 1234")]
        [InlineData("changeProductInfo pipe1X mail@gmail.com 1234")]
        [InlineData("changeMyInfo phonenumber John")]
        [InlineData("changeClienstInfo phonenumber +380500789546")]
        [InlineData("changeClientsInfo mail@gmail.com phonenumber mail@gmail.com")]
        [InlineData("showAllOrders 15")]
        [InlineData("showAllProducts pipe")]
        [InlineData("addNewProduct 15")]
        [InlineData("clearMyCart pipe1X")]
        [InlineData("removeProduct 125@ 12")]
        [InlineData("removeFromCart 125@ 12")]
        [InlineData("editItemInCart 125@ addSomeMore")]
        [InlineData("signUpAdmin vital@gmail.com qQ123 qQ123 WELDERPOINT")]

        public void NotValidate(string command)
        {
            Assert.False(command.IsValidCommand());
        }
    }
}
