using ConsoleShop.Data;
using ConsoleShop.Data.Types;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;
using static ConsoleShop.Data.Types.Order;
using static ConsoleShop.Data.Types.User;

namespace ConsoleShop.Tests.ConsoleShop.Data
{
    [Collection("IData Collection")]
    public class LazyDataSingletonShould : IClassFixture<SystemRepositoryFixture>
    {
        private readonly ITestOutputHelper _testOtput;
        private readonly SystemRepositoryFixture _fixture;

        public LazyDataSingletonShould(ITestOutputHelper outputHelper, SystemRepositoryFixture systemRepositoryFixture)
        {
            this._testOtput = outputHelper;
            this._fixture = systemRepositoryFixture;
        }

        [Theory]
        [InlineData("pipe1X")]
        [InlineData("pipe2X")]
        [InlineData("pipe3X")]

        public void searchByProductIdReturnTrue(string productID)
        {
            Assert.True(_fixture.Sut.searchByProductId(productID).ID.Equals(productID));
        }


        [Theory]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("jeff_vintar@gmail.com")]
        [InlineData("chi_mcbride@aol.com")]

        public void PerformACheckIfAClientReturnTrue(string email)
        {
            Assert.True(_fixture.Sut.IsClient(email));
        }

        [Theory]
        [InlineData("user_56@aol.com")]
        [InlineData("user_68@aol.com")]
        [InlineData("user_70@gmail.com")]
        public void PerformACheckIfAClientReturnFalse(string email)
        {
            Assert.False(_fixture.Sut.IsClient(email));
        }

        [Theory]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("jeff_vintar@gmail.com")]
        [InlineData("chi_mcbride@aol.com")]
        public void ReturnCorrectClient(string email)
        {
            Assert.True(_fixture.Sut.GetClient(email).Email.Equals(email));
        }

        [Theory]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("chi_mcbride@aol.com")]
        public void AddClientReturnWarning(string email)
        {
            Assert.DoesNotContain("welcome", _fixture.Sut.AddClient(email, new User("testUser1", "qQ123!!!", UserStatuses.Client)));
        }

        [Theory]
        [InlineData("vital@yahoo.com")]
        [InlineData("john_mcgee@aol.com")]
        public void AddClientReturnWelcome(string email)
        {
            Assert.Contains("welcome", _fixture.Sut.AddClient(email, new User("testUser1", "qQ123!!!", UserStatuses.Client)));
        }


        [Theory]
        [InlineData("chi_mcbride@aol.com")]
        [InlineData("akiva_goldsman@yahoo.com")]
        public void AddOrderReturnTrue(string email)
        {
            User user = _fixture.Sut.GetClient(email);
            _testOtput.WriteLine($"User: {user.LastName}");

            Order newOrder = new Order(user.UserID, $"{email}_testOrder", DateTime.Now.Date, 10000, OrderStatuses.New, new List<OrderItem>() { new OrderItem(_fixture.Sut.searchByProductId("pipe1X"), 10) });
            Assert.False(_fixture.Sut.InOrders(newOrder.OrderId));
            _fixture.Sut.AddOrder(email, newOrder);
            _testOtput.WriteLine($"{user.LastName} orders: {_fixture.Sut.HasOrders(user.Email)}");
            foreach (var note in newOrder.GetInfo())
                _testOtput.WriteLine($"{note.Key}: {note.Value}");
            Assert.True(_fixture.Sut.InOrders(newOrder.OrderId));
        }

        [Theory]
        [InlineData("akiva_goldsman@yahoo.com_testOrder")]
        [InlineData("chi_mcbride@aol.com_testOrder")]
        public void InOrdersReturnTrue(string orderID)
        {
            Assert.True(_fixture.Sut.InOrders(orderID));
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com")]
        [InlineData("will_smith@aol.com")]
        public void HasOrdersReturnTrue(string email)
        {
            Assert.True(_fixture.Sut.HasOrders(email));
        }

        [Theory]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("jeff_vintar@gmail.com")]
        [InlineData("john@gmail.com")]
        public void HasOrdersReturnFalse(string email)
        {
            Assert.False(_fixture.Sut.HasOrders(email));
        }

        [Theory]
        [InlineData("product_56")]
        [InlineData("product_57")]
        [InlineData("product_58")]
        public void IsInProductsReturnFalse(string email)
        {
            Assert.False(_fixture.Sut.HasOrders(email));
        }

        [Theory]
        [InlineData("pipe1X")]
        [InlineData("pipe2X")]
        [InlineData("pipe3X")]
        public void IsInProductsReturnTrue(string productID)
        {
            Assert.True(_fixture.Sut.IsInProducts(productID));
        }

        [Theory]
        [InlineData("order1X", "user1X")]
        [InlineData("order2X", "user1X")]
        [InlineData("order3X", "user1X")]
        public void GetOwnerIdReturnTrue(string productID, string ownerID)
        {
            Assert.True(_fixture.Sut.GetOwnerId(productID).Equals(ownerID));
        }

        [Theory]
        [InlineData("order4X", "user1X")]
        [InlineData("order5X", "user1X")]
        [InlineData("order6X", "user1X")]
        public void GetOwnerIdReturnFalse(string productID, string ownerID)
        {
            Assert.False(_fixture.Sut.GetOwnerId(productID).Equals(ownerID));
        }

        [Theory]
        [InlineData("milk")]
        [InlineData("bread")]
        [InlineData("whiskey")]
        public void searchByNameReturnFalse(string productName)
        {
            Assert.DoesNotContain("Result", _fixture.Sut.searchByName(productName).Keys);
        }

        [Theory]
        [InlineData("pipe")]
        [InlineData("Electrodes")]
        [InlineData("bar")]
        public void searchByNameReturnTrue(string productName)
        {
            Assert.Contains(_fixture.Sut.searchByName(productName).Values, productInfo => productInfo.Contains(productName));
        }

        [Fact]
        public void AddProductReturnTrue()
        {
            Product newProduct1 = new Product() { ID = "AddProductReturnTrue_1", Name = "Milk", Category = "Dairy", Description = "Dairy", Price = 4000, Discount = 4 };
            Product newProduct2 = new Product() { ID = "AddProductReturnTrue_2", Name = "Milk", Category = "Dairy", Description = "Dairy", Price = 4000, Discount = 4 };
            Product newProduct3 = new Product() { ID = "AddProductReturnTrue_3", Name = "Milk", Category = "Dairy", Description = "Dairy", Price = 4000, Discount = 4 };

            foreach (var product in new Product[] { newProduct1, newProduct2, newProduct3 })
            {
                _fixture.Sut.AddProduct(product.ID, product);
                Assert.True(_fixture.Sut.IsInProducts(product.ID));
            }
        }


        [Theory]
        [InlineData("order6X", OrderStatuses.New)]
        [InlineData("order1X", OrderStatuses.Received)]
        [InlineData("order2X", OrderStatuses.Completed)]
        public void setOrderStatusReturnTrueStatus(string orderID, OrderStatuses orderStatus)
        {
            _fixture.Sut.setOrderStatus(orderID, OrderStatuses.PaymentReceived);
            Order order = _fixture.Sut.ShopsOrders.Values.SelectMany(clientOrders => clientOrders.Where(order => order.OrderId == orderID)).First();
            Assert.NotEqual(orderStatus, order.OrderStatus);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com")]
        [InlineData("will_smith@aol.com")]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("jeff_vintar@gmail.com")]
        public void showMyInfoReturnTrueStatus(string email)
        {
            Dictionary<string, string> myInfo = _fixture.Sut.showMyInfo(email);
            Assert.Contains("UserID", myInfo.Keys);
            Assert.Contains("UserStatus", myInfo.Keys);
            Assert.Contains("FirstName", myInfo.Keys);
            Assert.Contains("LastName", myInfo.Keys);
            Assert.Contains("BirthDate", myInfo.Keys);
            Assert.Contains("PhoneNumber", myInfo.Keys);
            Assert.Contains("Email", myInfo.Keys);
        }

        [Theory]
        [InlineData("will_smith@aol.com", "FirstName", "John")]
        public void changeMyInfoFirstName<T>(string email, string fieldName, T value)
        {
            User previousUser = _fixture.Sut.GetClient(email).Clone() as User;
            foreach (var item in previousUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeMyInfo(email, fieldName, value);
            User newUser = _fixture.Sut.GetClient(email);
            foreach (var item in newUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousUser.FirstName, newUser.FirstName);
        }

        [Theory]
        [InlineData("will_smith@aol.com", "PhoneNumber", "+380500894500")]
        public void changeMyInfoReturnPhoneNumber<T>(string email, string fieldName, T value)
        {
            User previousUser = _fixture.Sut.GetClient(email).Clone() as User;
            foreach (var item in previousUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeMyInfo(email, fieldName, value);
            User newUser = _fixture.Sut.GetClient(email);
            foreach (var item in newUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousUser.PhoneNumber, newUser.PhoneNumber);
        }

        [Theory]
        [InlineData("will_smith@aol.com", "Email", "john_brown@gmail.com")]
        public void changeMyInfoReturnEmail<T>(string email, string fieldName, T value)
        {
            User previousUser = _fixture.Sut.GetClient(email).Clone() as User;
            foreach (var item in previousUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeMyInfo(email, fieldName, value);
            _testOtput.WriteLine(value as String);
            User newUser = _fixture.Sut.GetClient(value as String);
            foreach (var item in newUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousUser.Email, newUser.Email);
        }

        [Theory]
        [InlineData("john_brown@gmail.com", "Email", "will_smith@aol.com")]
        public void changeMyInfoReturnEmailRollBack<T>(string email, string fieldName, T value)
        {
            User previousUser = _fixture.Sut.GetClient(email).Clone() as User;
            foreach (var item in previousUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeMyInfo(email, fieldName, value);
            User newUser = _fixture.Sut.GetClient(value as String);
            foreach (var item in newUser.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousUser.Email, newUser.Email);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com")]
        [InlineData("akiva_goldsman@yahoo.com")]
        [InlineData("jeff_vintar@gmail.com")]
        public void showClientInfoReturnTrueStatus(string email)
        {
            Dictionary<string, string> myInfo = _fixture.Sut.showMyInfo(email);
            Assert.Contains("UserID", myInfo.Keys);
            Assert.Contains("UserStatus", myInfo.Keys);
            Assert.Contains("FirstName", myInfo.Keys);
            Assert.Contains("LastName", myInfo.Keys);
            Assert.Contains("BirthDate", myInfo.Keys);
            Assert.Contains("PhoneNumber", myInfo.Keys);
            Assert.Contains("Email", myInfo.Keys);
        }

        [Theory]
        [InlineData("pipe1X", "Name", "Pipe")]
        public void changeProductInfoName<T>(string productID, string fieldName, T value)
        {
            Product previousProduct = _fixture.Sut.searchByProductId(productID).Clone() as Product;
            foreach (var item in previousProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeProductInfo(productID, fieldName, value);
            Product newProduct = _fixture.Sut.searchByProductId(productID);
            foreach (var item in newProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousProduct.Name, newProduct.Name);
        }

        [Theory]
        [InlineData("pipe1X", "Price", 20000)]
        public void changeProductInfoReturnPrice<T>(string productID, string fieldName, T value)
        {
            Product previousProduct = _fixture.Sut.searchByProductId(productID).Clone() as Product;
            foreach (var item in previousProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeProductInfo(productID, fieldName, value);
            Product newProduct = _fixture.Sut.searchByProductId(productID);
            foreach (var item in newProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousProduct.Price, newProduct.Price);
        }

        [Theory]
        [InlineData("pipe1X", "Discount", 10)]
        public void changeProductInfoDiscount<T>(string productID, string fieldName, T value)
        {
            Product previousProduct = _fixture.Sut.searchByProductId(productID).Clone() as Product;
            foreach (var item in previousProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            _fixture.Sut.changeProductInfo(productID, fieldName, value);
            Product newProduct = _fixture.Sut.searchByProductId(productID);
            foreach (var item in newProduct.GetInfo())
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(previousProduct.Discount, newProduct.Discount);
        }

        [Theory]
        [InlineData("AddProductReturnTrue_1")]
        [InlineData("AddProductReturnTrue_2")]
        [InlineData("AddProductReturnTrue_3")]
        public void removeProduct(string productID)
        {
            _fixture.Sut.removeProduct(productID);
            Assert.False(_fixture.Sut.IsInProducts(productID));
        }

        [Theory]
        [InlineData(OrderStatuses.New, 2)]
        [InlineData(OrderStatuses.CanceledByAdministrator, 1)]
        public void showAllOrders(OrderStatuses orderStatus, int ordersCount)
        {
            Dictionary<string, string> result = _fixture.Sut.showAllOrders(orderStatus);
            _testOtput.WriteLine(result.Count((pair) => pair.Key.Contains("Order") && pair.Key.Contains("Client")).ToString());
            foreach (var item in result)
            {
                _testOtput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.True(result.Count((pair) => pair.Key.Contains("Order") && pair.Key.Contains("Client")) == ordersCount);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", 4)]
        [InlineData("will_smith@aol.com", 3)]
        [InlineData("jeff_vintar@gmail.com", 0)]
        public void showClientOrders(string email, int ordersCount)
        {
            Dictionary<string, string> result = _fixture.Sut.showClientOrders(email);
            _testOtput.WriteLine(result.Count((pair) => pair.Key.Contains("Order") && pair.Key.Contains("Client")).ToString());
            Assert.True(result.Count((pair) => pair.Key.Contains("Order") && pair.Key.Contains("Client"))  == ordersCount);
        }

        
    }
}
