﻿using ConsoleShop.Data.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit.Abstractions;

namespace ConsoleShop.Tests.ConsoleShop.Data
{
    public class SystemRepositoryFixture 
    {
        public LazyDataSingleton Sut { get; private set; }

        public SystemRepositoryFixture()
        {
            //~Thread.Sleep(10000)~
            this.Sut = LazyDataSingleton.Instance;            
        }
        
    }
}
