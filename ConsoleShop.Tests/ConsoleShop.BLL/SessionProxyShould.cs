﻿using ConsoleShop.Data.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using ConsoleShop.Domain.Interfaces;
using static ConsoleShop.Data.Types.User;
using System.Linq;
using ConsoleShop.Services;
using ConsoleShop.Data.Types;

namespace ConsoleShop.Tests.ConsoleShop.BLL
{
    public class SessionProxyShould
    {
        private readonly Mock<ISession> _sessionMock;
        private readonly SessionProxy sut;
        private readonly ITestOutputHelper _testOutput;
        private readonly IData _systemRepo = LazyDataSingleton.Instance;

        public SessionProxyShould(ITestOutputHelper testOutput)
        {
            this._sessionMock = new Mock<ISession>();

            this._sessionMock.SetupAllProperties();            

            this._sessionMock.Setup(method => method.signIn("chi_mcbride@aol.com", "BrIghT_61_23")).Returns("Welcome");
            this._sessionMock.Setup(method => method.signIn("will_smith@aol.com", "$IRoBot_68_25")).Returns("Welcome");
            this._sessionMock.Setup(method => method.signIn("jeff_vintar@gmail.com", "ViN_#6890")).Returns("Welcome");
            
            sut = new SessionProxy(_sessionMock.Object);
            this._testOutput = testOutput;

        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "order4X", "Received")]
        [InlineData("chi_mcbride@aol.com", "order5X", "CanceledByUser")]
        public void setOrderStatusClientsByClientSuccess(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(true);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(false);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            this._sessionMock.Setup(method => method.GetOwnerId(orderID)).Returns(_systemRepo.GetOwnerId(orderID));
            
            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Success", result.Keys);
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Once);

        }
       
        [Theory]
        [InlineData("chi_mcbride@aol.com", "order4X", "CanceledByAdministrator")]
        [InlineData("chi_mcbride@aol.com", "order5X", "Completed")]
        public void setOrderStatusClientsByClientWrongStatusFails(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(true);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(false);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            this._sessionMock.Setup(method => method.GetOwnerId(orderID)).Returns(_systemRepo.GetOwnerId(orderID));

            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Warning", result.Keys.First());
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Never);
        }
        
        [Theory]
        [InlineData("chi_mcbride@aol.com", "order1X", "Received")]
        [InlineData("chi_mcbride@aol.com", "order2X", "CanceledByUser")]
        public void setOrderStatusClientsByAnotherClientFails(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(true);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(false);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            this._sessionMock.Setup(method => method.GetOwnerId(orderID)).Returns(_systemRepo.GetOwnerId(orderID));

            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Warning", result.Keys.First());
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Never);
        }
        
        [Theory]
        [InlineData("will_smith@aol.com", "order4X", "Payment Received")]
        [InlineData("will_smith@aol.com", "order5X", "CanceledByAdministrator")]
        public void setOrderStatusClientsByAdminSuccess(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(false);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(true);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            

            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Success", result.Keys);
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Once);
        }
        
        [Theory]
        [InlineData("will_smith@aol.com", "order4X", "Received")]
        [InlineData("will_smith@aol.com", "order5X", "CanceledByUser")]
        public void setOrderStatusClientsByAdminWrongStatusFails(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(false);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(true);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            this._sessionMock.Setup(method => method.GetOwnerId(orderID)).Returns(_systemRepo.GetOwnerId(orderID));

            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Warning", result.Keys.First());
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Never);
        }

        [Theory]
        [InlineData("will_smith@aol.com", "order2X", "Received")]
        [InlineData("will_smith@aol.com", "order3X", "CanceledByUser")]
        [InlineData("will_smith@aol.com", "order1X", "Completed")]
        public void setOrderStatusAdminsByAdminSuccess(string email, string orderID, string _orderStatus)
        {
            this._sessionMock.Setup(prop => prop.GetLoggedInUser).Returns(_systemRepo.GetClient(email));

            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Client)).Returns(false);
            this._sessionMock.Setup(method => method.IsAllowed(UserStatuses.Administrator)).Returns(true);
            this._sessionMock.Setup(method => method.setOrderStatus(orderID, _orderStatus)).Returns(new Dictionary<string, string>() { { "Success", "Success" } });
            this._sessionMock.Setup(method => method.InOrders(orderID)).Returns(true);
            this._sessionMock.Setup(method => method.GetOwnerId(orderID)).Returns(_systemRepo.GetOwnerId(orderID));

            Dictionary<string, string> result = sut.setOrderStatus(orderID, _orderStatus);
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Contains("Success", result.Keys);
            this._sessionMock.Verify(method => method.setOrderStatus(orderID, _orderStatus), Times.Once);
        }
    }
}
