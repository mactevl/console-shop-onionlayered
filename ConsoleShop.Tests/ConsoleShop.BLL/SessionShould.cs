﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Moq;
using ConsoleShop.Data.Interfaces;
using ConsoleShop.Data.Types;
using static ConsoleShop.Data.Types.User;
using System.Linq;
using static ConsoleShop.Data.Types.Order;
using static ConsoleShop.Presentation.Extensions.StringExtensions;
using ConsoleShop.Services;

namespace ConsoleShop.Tests.ConsoleShop.BLL
{
    public class SessionShould
    {
        private readonly Mock<IData> _systemRepoMock;
        private readonly Session sut;
        private readonly ITestOutputHelper _testOutput;
        private readonly IData _systemRepo = LazyDataSingleton.Instance;
        public SessionShould(ITestOutputHelper testOutput)
        {
            this._systemRepoMock = new Mock<IData>();
            this._systemRepoMock.SetupAllProperties();
            this._systemRepoMock.Setup(method => method.GetClient("chi_mcbride@aol.com")).Returns(_systemRepo.GetClient("chi_mcbride@aol.com"));

            this._systemRepoMock.Setup(method => method.GetClient("will_smith@aol.com")).Returns(_systemRepo.GetClient("will_smith@aol.com"));
            this._systemRepoMock.Setup(method => method.IsClient("chi_mcbride@aol.com")).Returns(true);
            this._systemRepoMock.Setup(method => method.IsClient("will_smith@aol.com")).Returns(true);
            this._systemRepoMock.Setup(method => method.IsClient("mark@aol.com")).Returns(true);
            this._systemRepoMock.Setup(method => method.IsClient("vital@aol.com")).Returns(true);
            this._systemRepoMock.Setup(method => method.HasOrders("chi_mcbride@aol.com")).Returns(_systemRepo.HasOrders("chi_mcbride@aol.com"));
            this._systemRepoMock.Setup(method => method.HasOrders("will_smith@aol.com")).Returns(_systemRepo.HasOrders("will_smith@aol.com"));


            sut = new Session(_systemRepoMock.Object);
            this._testOutput = testOutput;

        }

        [Fact]
        public void showMyOrdersWarning()
        {
            _systemRepoMock.Setup(method => method.showClientOrders(It.IsAny<string>())).Returns(new Dictionary<string, string>() { { "Result: ", "You have to sign in first." } });

            sut.showMyOrders();

            _systemRepoMock.Verify(method => method.showClientOrders(It.IsAny<string>()), Times.Never);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "BrIghT_61_23")]
        [InlineData("will_smith@aol.com", "$IRoBot_68_25")]
        public void showMyOrdersSuccess(string email, string password)
        {
            string result = sut.signIn(email, password);
            _systemRepoMock.Setup(method => method.showClientOrders(It.IsAny<string>())).Returns(_systemRepo.showMyOrders(email));

            sut.showMyOrders();
            Assert.Contains("Welcome", result);
            _systemRepoMock.Verify(method => method.showClientOrders(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void signOutWarning()
        {
            string result = sut.signOut();
            Assert.Contains("You have to be signed in first!", result);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "BrIghT_61_23")]
        [InlineData("will_smith@aol.com", "$IRoBot_68_25")]
        public void signOutSuccess(string email, string password)
        {
            sut.signIn(email, password);

            string result = sut.signOut();

            Assert.Contains("Looking foreward to see you again", result);
        }

        [Theory]
        [InlineData("Electrodes")]
        [InlineData("Bar")]
        [InlineData("Pipe")]
        public void searchByNameSuccess(string productName)
        {
            _systemRepoMock.Setup(method => method.searchByName(It.IsAny<string>())).Returns(_systemRepo.searchByName(productName));

            Dictionary<string, string> result = sut.searchByName(productName);
            _systemRepoMock.Verify(method => method.searchByName(It.IsAny<string>()), Times.Once);
            Assert.Contains(productName, result.Values);
        }

        [Theory]
        [InlineData("milk")]
        [InlineData("bread")]
        [InlineData("whiskey")]
        public void searchByNameWarning(string productName)
        {
            _systemRepoMock.Setup(method => method.searchByName(It.IsAny<string>())).Returns(new Dictionary<string, string>() { { "Result: ", "No results were found that match your request!" } });

            Dictionary<string, string> result = sut.searchByName(productName);
            _systemRepoMock.Verify(method => method.searchByName(It.IsAny<string>()), Times.Once);
            Assert.Contains("Result", result.Keys.First());
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "BrIghT_61_23", "BrIghT_61_23")]
        [InlineData("will_smith@aol.com", "$IRoBot_68_25", "$IRoBot_68_25")]
        public void maySignUpWarning(string email, string password, string passwordConfirmation)
        {
            string result = sut.maySignUp(email, password, passwordConfirmation);

            _systemRepoMock.Verify(method => method.IsClient(email), Times.Once);
            Assert.Equal("That email had already been registered!", result);
        }

        [Theory]
        [InlineData("testUserJohn", "John", "McGee", "07/25/1998", "+380682340008", "vital@aol.com", "BrIghT_61_23", UserStatuses.Client)]
        [InlineData("testUserJack", "Jack", "McGee", "07/25/1998", "+380682340008", "mark@aol.com", "$IRoBot_68_25", UserStatuses.Client)]
        public void signUpSuccess(string UserID, string FirstName, string LastName, string BirthDate, string PhoneNumber, string Email, string Password, UserStatuses UserStatus)
        {
            User newUser = new User(UserID, UserStatus, FirstName, LastName, DateTime.Parse(BirthDate), PhoneNumber, Email, Password);

            _systemRepoMock.Setup(method => method.AddClient(newUser.Email, newUser)).Returns("We are glad to welcome You in our family! Feel free to explore everything in here.");

            _systemRepoMock.Setup(method => method.GetClient(newUser.Email)).Returns(newUser);

            _systemRepoMock.Setup(prop => prop.ShopsUsersCount).Returns(_systemRepo.ShopsUsersCount);

            _systemRepoMock.Setup(method => method.GetClient(newUser.Email)).Returns(newUser);


            Dictionary<string, string> personalInfo = new Dictionary<string, string>();

            personalInfo.Add("FirstName: ", FirstName);
            personalInfo.Add("LastName: ", LastName);
            personalInfo.Add("BirthDate (example: 05/18/2022): ", BirthDate);
            personalInfo.Add("PhoneNumber: ", PhoneNumber);
            personalInfo.Add("Email: ", Email);
            personalInfo.Add("Password: ", Password);
            personalInfo.Add("User Status: ", UserStatus.ToString());

            string result = sut.signUp(personalInfo);

            Assert.Contains("WELDERPOINT", result);
        }

        [Theory]
        [InlineData("product_1", 10)]
        [InlineData("product_2", 20)]
        [InlineData("product_3", 30)]
        public void addToCartSuccess(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string addToCartResult = sut.addToCart(productID, amount);

            Assert.Contains("Welcome", signInResult);
            _testOutput.WriteLine(addToCartResult);
            Assert.Contains("items has been added to your Cart", addToCartResult);

            _systemRepoMock.Verify(method => method.searchByProductId(productID), Times.AtLeastOnce);

        }

        [Theory]
        [InlineData("bar1X", 10)]
        [InlineData("bar2X", 20)]
        [InlineData("bar3X", 30)]
        public void addToCartFails(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(false);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string addToCartResult = sut.addToCart(productID, amount);
            _testOutput.WriteLine(addToCartResult);
            Assert.DoesNotContain("items has been added to your Cart", addToCartResult);

            _systemRepoMock.Verify(method => method.IsInProducts(productID), Times.Once);
            _systemRepoMock.Verify(method => method.searchByProductId(productID), Times.Never);
        }


        [Theory]
        [InlineData("product_1", 10)]
        [InlineData("product_2", 20)]
        [InlineData("product_3", 30)]
        public void showMyCartSuccess(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string addToCartResult = sut.addToCart(productID, amount);

            Dictionary<string, string> result = sut.showMyCart();
            Assert.True(result.Keys.Count(key => key.Contains(productID)) >= 1);
        }

        [Theory]
        [InlineData("bar1X", 10)]
        [InlineData("bar2X", 20)]
        [InlineData("bar3X", 30)]
        public void showMyCartFails(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(false);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string addToCartResult = sut.addToCart(productID, amount);
            _testOutput.WriteLine(addToCartResult);

            Dictionary<string, string> result = sut.showMyCart();
            Assert.DoesNotContain(productID, result.Keys);
        }

        [Theory]
        [InlineData("product_1", 10, 20)]
        [InlineData("product_2", 20, 30)]
        [InlineData("product_3", 30, 40)]
        public void editItemInCartSuccess(string productID, int amount, int newAmount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            sut.addToCart(productID, amount);

            Dictionary<string, string> baseResult = sut.showMyCart();

            Dictionary<string, string> modifiedResult = sut.editItemInCart(productID, newAmount);
            foreach (var item in modifiedResult)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }

            Assert.NotEqual(baseResult[baseResult.Keys.First(note => note.Contains("Amount"))], modifiedResult[modifiedResult.Keys.First(note => note.Contains("Amount"))]);
        }

        [Theory]
        [InlineData("product_1", 10)]
        [InlineData("product_2", 20)]
        [InlineData("product_3", 30)]
        public void removeFromCartSuccess(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            sut.addToCart(productID, amount);

            Dictionary<string, string> baseResult = sut.showMyCart();

            Dictionary<string, string> modifiedResult = sut.removeFromCart(productID);
            foreach (var item in modifiedResult)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.NotEqual(baseResult.Count, modifiedResult.Count);
        }


        [Theory]
        [InlineData("product_1", 10)]
        public void clearMyCartSuccess(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));

            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string addToCartResult = sut.addToCart(productID, amount);

            Dictionary<string, string> result = sut.showMyCart();
            Assert.Contains(productID, result.Keys.First(key => key.Contains(productID)));

            sut.clearMyCart();
            Assert.DoesNotContain(productID, result.Keys);
        }

        [Fact]
        public void clearMyCartFails()
        {
            string signInResult = sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            string clearMyCartResult = sut.clearMyCart();
            _testOutput.WriteLine(signInResult);

            sut.showMyCart();
            Assert.Contains("There is nothing in your cart yet!", clearMyCartResult);
        }

        [Theory]
        [InlineData("product_1", 10)]
        [InlineData("product_2", 20)]
        [InlineData("product_3", 30)]
        public void makeOrderSuccess(string productID, int amount)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));
            _systemRepoMock.Setup(prop => prop.ShopsOrdersCount).Returns(_systemRepo.ShopsOrdersCount);
            _systemRepoMock.Setup(method => method.AddOrder(It.IsAny<string>(), It.IsAny<Order>())).Returns(true);

            sut.signIn("chi_mcbride@aol.com", "BrIghT_61_23");
            sut.addToCart(productID, amount);

            sut.makeOrder();
            _systemRepoMock.Verify(method => method.AddOrder(It.IsAny<string>(), It.IsAny<Order>()), Times.Once);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "BrIghT_61_23")]
        [InlineData("will_smith@aol.com", "$IRoBot_68_25")]
        public void showMyInfoSuccess(string email, string password)
        {
            _systemRepoMock.Setup(method => method.showMyInfo(email)).Returns(_systemRepo.showMyInfo(email));

            sut.signIn(email, password);

            Dictionary<string, string> result = sut.showMyInfo();

            Assert.Contains("Email", result.Keys);
        }

        [Fact]
        public void showMyInfoFails()
        {
            Dictionary<string, string> result = sut.showMyInfo();

            Assert.Contains("You have to sign in first!", result.Values);
        }

        [Theory]
        [InlineData("chi_mcbride@aol.com", "BrIghT_61_23", "PhoneNumber", "+380500894678")]
        [InlineData("will_smith@aol.com", "$IRoBot_68_25", "FirstName", "Jack")]
        public void changeMyInfoSuccess(string email, string password, string fieldName, string value)
        {
            sut.signIn(email, password);

            Dictionary<string, string> baseResult = sut.showMyInfo();

            Dictionary<string, string> modifiedResult = sut.changeMyInfo(fieldName, value);

            Assert.NotEqual(baseResult, modifiedResult);
        }

        [Theory]
        [InlineData(OrderStatuses.New, 5)]
        [InlineData(OrderStatuses.Received, 1)]
        [InlineData(OrderStatuses.Completed, 1)]
        public void showAllOrdersSuccess(OrderStatuses orderStatus, int ordersNumber)
        {
            _systemRepoMock.Setup(method => method.showAllOrders(orderStatus)).Returns(_systemRepo.showAllOrders(orderStatus));

            Dictionary<string, string> result = sut.showAllOrders(orderStatus.ToString());
            foreach (var item in result)
            {
                _testOutput.WriteLine(item.Key + ": " + item.Value);
            }
            Assert.Equal(ordersNumber, result.Keys.Count(key => key.Contains("Order ID") && key.Contains("Client")));
        }

        [Fact]
        public void showAllProductsSuccess()
        {
            _systemRepoMock.Setup(method => method.showAllProducts()).Returns(_systemRepo.showAllProducts());

            Dictionary<string, string> result = sut.showAllProducts();

            Assert.True(result.Count > 1);
            _systemRepoMock.Verify(method => method.showAllProducts(), Times.Once);

        }

        [Theory]
        [InlineData("order1X", OrderStatuses.Completed)]
        [InlineData("order2X", OrderStatuses.Received)]
        [InlineData("order3X", OrderStatuses.New)]
        public void setOrderStatusSuccess(string orderID, OrderStatuses orderStatus)
        {
            _systemRepoMock.Setup(method => method.setOrderStatus(orderID, orderStatus)).Returns(_systemRepo.setOrderStatus(orderID, orderStatus));

            Dictionary<string, string> result = sut.setOrderStatus(orderID, orderStatus.ToString());

            Assert.Contains(orderStatus.ToString(), result.Values);
            _systemRepoMock.Verify(method => method.setOrderStatus(orderID, orderStatus), Times.Once);
        }

        [Theory]
        [InlineData("Paton 4mm", "Electrodes", "Paton 4mm. 2.5 kg", "25000", "5")]
        [InlineData("Paton 5mm", "Electrodes", "Paton 5mm. 2.5 kg", "30000", "4")]
        [InlineData("Paton 6mm", "Electrodes", "Paton 6mm. 2.5 kg", "35000", "3")]
        public void addNewProductSuccess(string Name, string Category, string Description, string Price, string Discount)
        {
            _systemRepoMock.Setup(prop => prop.ShopsProductsCount).Returns(_systemRepo.ShopsProductsCount);
            _systemRepoMock.Setup(method => method.AddProduct(It.IsAny<string>(), It.IsAny<Product>())).Returns(true);

            Dictionary<string, string> productState = new Dictionary<string, string>() { 
                { "Name", Name}, 
                { "Category", Category }, 
                { "Description", Description },
                { "Price", Price },
                { "Discount", Discount }};

            Dictionary<string, string> result = sut.addNewProduct(productState);
            Assert.True(result.Count > 1);

            _systemRepoMock.Verify(method => method.AddProduct(It.IsAny<string>(), It.IsAny<Product>()), Times.Once);
        }

        [Theory]
        [InlineData("product_55")]
        [InlineData("product_54")]
        public void removeProductSuccess(string productID)
        {
            _systemRepoMock.Setup(method => method.removeProduct(productID)).Returns(true);

            sut.removeProduct(productID);

            _systemRepoMock.Verify(method => method.removeProduct(productID), Times.Once);
        }

        [Theory]
        [InlineData("product_55", "Name", "product_55")]
        [InlineData("product_54", "Price", "18900")]
        public void changeProductInfoSuccess(string productID, string fieldName, string value)
        {
            _systemRepoMock.Setup(method => method.IsInProducts(productID)).Returns(true);
            _systemRepoMock.Setup(method => method.searchByProductId(productID)).Returns(_systemRepo.searchByProductId(productID));
            var baseProduct = (Product)_systemRepo.searchByProductId(productID).Clone();
            sut.changeProductInfo(productID, fieldName, value);
            var modifiedProduct = _systemRepo.searchByProductId(productID);

            _systemRepoMock.Verify(method => method.searchByProductId(productID), Times.AtLeastOnce);
            Assert.NotEqual(baseProduct.GetInfo().First(pair => pair.Key.Contains(fieldName)).Value, modifiedProduct.GetInfo().First(pair => pair.Key.Contains(fieldName)).Value);
        }

        [Fact]
        public void checkCredentialsSuccess()
        {
            _systemRepoMock.Setup(method => method.MasterKey("WELDERPOINT")).Returns(true);

            sut.checkCredentials("WELDERPOINT");

            _systemRepoMock.Verify(method => method.MasterKey("WELDERPOINT"), Times.Once);
        }

        [Fact]
        public void checkCredentialsFails()
        {
            _systemRepoMock.Setup(method => method.MasterKey("wrong master key")).Returns(false);

            sut.checkCredentials("wrong master key");

            _systemRepoMock.Verify(method => method.MasterKey("wrong master key"), Times.Once);
        }
    }
}
